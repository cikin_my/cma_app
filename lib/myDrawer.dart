import 'package:cma_app/pages/login-option.dart';
import 'package:cma_app/screen/supervisor/homepage.dart';
import 'package:cma_app/screen/supervisor/worker_add.dart';
import 'package:cma_app/screen/supervisor/worker_list.dart';
import 'package:flutter/material.dart';


class MyDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName:  Text(
              "Shukeri Salleh",
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w800,
                color: Colors.white,
              ),
            ),
            accountEmail:  Text(
              'Supervisor',
              style: TextStyle(
                letterSpacing: 3.0,
                fontSize: 13,
                fontWeight: FontWeight.w600,
                color: Colors.white60,

              ),
            ),
            currentAccountPicture: ClipRRect(
              borderRadius: BorderRadius.circular(110),
              child: Image.asset("assets/images/me.png", fit: BoxFit.cover,),
            ),
            decoration:  BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  colors: [Color.fromRGBO(55,213,214,1), Color.fromRGBO(54,9,109,1)]),
            ),
          ),
          ListTile(
            trailing:  Icon(Icons.person_add),
            title:  Text("Add  Worker"),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.of(context).push( MaterialPageRoute(
                  builder: (BuildContext context) =>  WorkerAdd()));
            },
          ),
          Divider(height: 1),
          ListTile(
            trailing:  Icon(Icons.people),
            title:  Text("View Worker List"),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.of(context).push( MaterialPageRoute(
                  builder: (BuildContext context) =>  WorkerList()));
            },
          ),
          Divider(
            height: 1,
          ),
          ListTile(
            trailing:  Icon(Icons.settings_power),
            title:  Text("Log Out"),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.of(context).push( MaterialPageRoute(
                  builder: (BuildContext context) =>  LoginOption()));
            },
          ),
        ],
      ),
    );
  }
}