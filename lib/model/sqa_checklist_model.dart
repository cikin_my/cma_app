class sqa {
  String title;
  String subtitle;
  String id;
  String checked;

  sqa(this.checked, this.id, this.subtitle, this.title);

  sqa.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    subtitle = json['subtitle'];
    id = json['id'];
    checked = json['checked'];
  }
}
