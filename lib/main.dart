import 'package:cma_app/screen/worker/add_hazard.dart';
import 'package:cma_app/screen/worker/hazard.dart';
import 'package:cma_app/screen/worker/sqa_form.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());
enum AuthMode { SUPERVISOR, WORKER,NON }

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: new ThemeData(primarySwatch: Colors.blue),
      debugShowCheckedModeBanner: false,
      home: Hazard_add() ,
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {




  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
