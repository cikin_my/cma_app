import 'package:cma_app/screen/supervisor/timetable.dart';
import 'package:flutter/material.dart';

class MyHero extends StatefulWidget {
  @override
  _MyHeroState createState() => _MyHeroState();
}

class _MyHeroState extends State<MyHero> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(

      body: Hero(
          tag: 'yourtag',
          child: GestureDetector(
              onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => Timetable())),
              child: FlutterLogo(
                size: 100.0,
              ))),
    );
  }
}

class HeroExamplePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Selected image'),
      ),
      body: Center(
        child: Hero(
            tag: 'yourtag',
            child: Text(" next : assignment details")
        ),
      ),
    );
  }
}