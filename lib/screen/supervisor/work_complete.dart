import 'package:cma_app/screen/supervisor/homepage.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../myDrawer_worker.dart';


class WorkComplete extends StatefulWidget {
  @override
  _WorkCompleteState createState() => _WorkCompleteState();
}

class _WorkCompleteState extends State<WorkComplete> {
  Widget _titleSection() {
    return Container(
      padding: const EdgeInsets.fromLTRB(32, 32, 32, 8),
      child: Row(
        children: [
          Expanded(
            /*1*/
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                /*2*/
                Chip(
                  backgroundColor: Colors.green,
                  label: Text(
                    'PO 454545445',
                    style: TextStyle(
                      fontFamily: "Lato",
                      color: Colors.white,
                    ),
                  ),
                ),
                Text(
                  'Kerja-kerja pembersihan dan selenggara am.',
                  style: TextStyle(
                      color: Colors.grey[800],
                      fontFamily: "Lato",
                      fontSize: 16),
                ),
              ],
            ),
          ),
          /*3*/
          Icon(
            Icons.star,
            color: Colors.red[500],
          ),
        ],
      ),
    );
  }

  Widget _textSection() {
    return Container(
        padding: const EdgeInsets.fromLTRB(32,8,32,8),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            ListTile(
              contentPadding: EdgeInsets.all(0.0),
              dense: true,
              leading: Icon(Icons.calendar_today),
              title: Text('Tarikh mula arahan kerja (Kontrak PO)'),
              subtitle: Text('10 April 2020'),
            ),
            ListTile(
              contentPadding: EdgeInsets.all(0.0),
              dense: true,
              leading: Icon(FontAwesomeIcons.calendarCheck),
              title: Text('Delivery Date (Kontrak PO)'),
              subtitle: Text('10 April 2020'),
            ),
            ListTile(
              contentPadding: EdgeInsets.all(0.0),
              dense: true,
              leading: Icon(Icons.business_center),
              title: Text('Vendor '),
              subtitle: Text('Bina Maju Sdn. Bhd.'),
            ),
            ListTile(
              contentPadding: EdgeInsets.all(0.0),
              dense: true,
              leading: Icon(FontAwesomeIcons.calendarTimes),
              title: Text('Tarikh Rancang Mula Kerja (Workplan)'),
              subtitle: Text('15 Mar 2020'),
            ),
            ListTile(
              contentPadding: EdgeInsets.all(0.0),
              dense: true,
              leading: Icon(Icons.info_outline),
              title: Text('Maklumat Lain'),
              subtitle: Text('Limited Access due to MCO.'),
            ),
            ListTile(
              contentPadding: EdgeInsets.all(0.0),
              dense: true,
              leading: Icon(Icons.today),
              title: Text('Tarikh Penugasan:'),
              subtitle: Text('10 April 2020'),
            ),
            ListTile(
              contentPadding: EdgeInsets.all(0.0),
              dense: true,
              leading: Icon(FontAwesomeIcons.userCircle),
              title: Text('Nama Staff'),
              subtitle: Text('Samad'),
            ),
            ListTile(
              contentPadding: EdgeInsets.all(0.0),
              dense: true,
              leading: Icon(Icons.input),
              title: Text('Tarikh Check In: '),
              subtitle: Text('30 April 2020'),
            ),
          ],
        ));
  }

  Widget _formSection() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.fromLTRB(18.0,0,18,0),
          child: TextFormField(
            style: TextStyle(
              fontSize: 17.0,
              color: Colors.black87,
            ),
            decoration:  InputDecoration(
              alignLabelWithHint: true,
              labelText: "Laporan Akhir Kerja",
              fillColor: Colors.white,
              contentPadding:
             EdgeInsets.all(20),
              border:  OutlineInputBorder(
                borderRadius:
                BorderRadius.circular(15.0),
                borderSide:  BorderSide(),
              ),
            ),
            keyboardType: TextInputType.multiline,
            maxLines: 5,
          ),
        ),

        Padding(
          padding: const EdgeInsets.fromLTRB(32, 5, 32, 8),
          child: SizedBox(
            width: double.infinity,
            child: RaisedButton.icon(
              onPressed: () {
                Navigator.of(context).push(new MaterialPageRoute(
                    builder: (BuildContext context) => Homepage()));
              },
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(20.0))),
              label: Text(
                'Hantar',
                style: TextStyle(
                    fontFamily: 'BellotaText',
                    color: Colors.white,
                    letterSpacing: 3),
              ),
              icon: Padding(
                padding: EdgeInsets.all(10.0),
                child: Icon(
                  Icons.send,
                ),
              ),
              textColor: Colors.white,
              splashColor: Colors.red.shade900,
              color: Colors.red.shade600,
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: new Text(
          "CMA Contractor",
        ),
        centerTitle: true,
        backgroundColor: Colors.blue.shade700,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context).push(new MaterialPageRoute(
                builder: (BuildContext context) => Homepage()));
          },
        ),
      ),
      drawer: MyWorkerDrawer(),
      body: ListView(
        children: [_titleSection(), _textSection(),_formSection()],
      ),
    );
  }
}
