
import 'package:cma_app/screen/supervisor/assignment_details.dart';
import 'package:cma_app/screen/supervisor/po_active.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:table_calendar/table_calendar.dart';
import 'dart:convert';

class Timetable extends StatefulWidget {
  @override
  _TimetableState createState() => _TimetableState();
}

class _TimetableState extends State<Timetable> {
  CalendarController _calendarController;
  Map<DateTime, List<dynamic>> _events;
  List<dynamic> _selectedEvents;
  TextEditingController _eventController;
  SharedPreferences prefs;


  @override
  void initState() {
    super.initState();
    _calendarController = CalendarController();
    _eventController = TextEditingController();
    _events = {};
    _selectedEvents = [];
    initPrefs();
  }
  initPrefs() async {
    prefs = await SharedPreferences.getInstance();
    setState(() {
      _events = Map<DateTime, List<dynamic>>.from(
          decodeMap(jsonDecode(prefs.get('events') ?? '{}')));
    });
  }
  // Encode Date Time Helper Method
  Map<String, dynamic> encodeMap(Map<DateTime, dynamic> map) {
    Map<String, dynamic> newMap = {};
    map.forEach((key, value) {
      newMap[key.toString()] = map[key];
    });
    return newMap;
  }

  // decode Date Time Helper Method
  Map<DateTime, dynamic> decodeMap(Map<String, dynamic> map) {
    Map<DateTime, dynamic> newMap = {};
    map.forEach((key, value) {
      newMap[DateTime.parse(key)] = map[key];
    });
    return newMap;
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            TableCalendar(
              events: _events,
              initialCalendarFormat: CalendarFormat.month,
              calendarStyle: CalendarStyle(
                markersColor: Colors.red.shade400,
                todayColor: Colors.orangeAccent,
                selectedColor: Theme.of(context).primaryColor,
                todayStyle:
                TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              ),
              headerStyle: HeaderStyle(
                formatButtonDecoration: BoxDecoration(
                  color: Colors.blueAccent,
                  borderRadius: BorderRadius.circular(20.0),
                ),
                formatButtonTextStyle: TextStyle(color: Colors.white),
                formatButtonShowsNext: false,
              ),
              startingDayOfWeek: StartingDayOfWeek.monday,
              calendarController: _calendarController,
              onDaySelected: (date, events) {
                setState(() {
                  print(date.toIso8601String());
                  _selectedEvents = events;
                });
              },
            ),
            ..._selectedEvents.map(
                  (event) => Padding(
                padding: EdgeInsets.fromLTRB(2.0, 0, 2.0, 0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Padding(
                      padding: EdgeInsets.all(5),
                      child: GestureDetector(
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.blue,
                            //color: Theme.of(context).primaryColor,
                            //border: Border.all(width: 0.3),
                            borderRadius: BorderRadius.circular(4.0),
                            border: Border.all(
                              width: 0.4,
                              color: Colors.blue.shade700,

                            ),
                          ),
                          child: Padding(
                            padding: EdgeInsets.all(15.0),
                            child: Text(
                              event,
                              style: TextStyle(
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.normal,
                                  color: Colors.white),
                            ),
                          ),

                        ),
                        onTap: (){
                          Navigator.of(context).pop();
          Navigator.of(context).push(MaterialPageRoute(
              builder: (BuildContext context) => AssignmentDetails()));
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.blue,
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
        onPressed: () {
        _showAddDialog();
//          Navigator.of(context).pop();
//          Navigator.of(context).push(MaterialPageRoute(
//              builder: (BuildContext context) => POActive()));
        },
      ),
    );
  }
  _showAddDialog() {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Tambah Tugas:'),
        content: TextFormField(
          controller: _eventController,
          style: TextStyle(
            fontSize: 14.0,
            color: Colors.black87,
          ),
          decoration: new InputDecoration(
            labelText: " nama tugasan baru",
            fillColor: Colors.white,
            contentPadding:
            new EdgeInsets.symmetric(
                vertical: 20.0,
                horizontal: 20.0),
            border: new OutlineInputBorder(
              borderRadius:
              new BorderRadius.circular(15.0),
              borderSide: new BorderSide(),
            ),
          ),
          keyboardType: TextInputType.multiline,
          maxLines: 4,
        ),
        actions: <Widget>[
          Row(
            children: <Widget>[
              FlatButton(
                color: Colors.blue,
                child: Text(
                  'Save',
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  setState(() {
                    if (_eventController.text.isEmpty) return;
                    if (_events[_calendarController.selectedDay] != null) {
                      _events[_calendarController.selectedDay]
                          .add(_eventController.text);
                    } else {
                      _events[_calendarController.selectedDay] = [
                        _eventController.text
                      ];
                    }
                    prefs.setString('events', jsonEncode(encodeMap(_events)));
                    Navigator.of(context).pop();
                    _eventController.clear();
                  });
                },
              ),
              SizedBox(
                width: 2.0,
              ),
              FlatButton(
                color: Colors.grey,
                child: Text(
                  'Cancel',
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          ),
        ],
      ),
    );
  }
}
