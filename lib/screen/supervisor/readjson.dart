import 'dart:convert';

import 'package:cma_app/screen/supervisor/assignment_details.dart';
import 'package:cma_app/screen/supervisor/test_hero.dart';
import 'package:flutter/material.dart';


class ReadMyJson extends StatefulWidget {
  @override
  _ReadMyJsonState createState() => _ReadMyJsonState();
}

class _ReadMyJsonState extends State<ReadMyJson> {
  List data;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          child: Center(
            // Use future builder and DefaultAssetBundle to load the local JSON file
            child: FutureBuilder(
                future: DefaultAssetBundle
                    .of(context)
                    .loadString('data/mydata.json'),
                builder: (context, snapshot) {
                  // Decode the JSON
                  var new_data = json.decode(snapshot.data.toString());

                  return ListView.builder(
                    itemCount: new_data == null ? 0 : new_data.length,
                    // Build the ListView
                    itemBuilder: (BuildContext context, int index) {
                      return Card(
                        elevation: 1.7,
                        child: new Padding(
                            padding: new EdgeInsets.all(10.0),
                            child: new Column(
                              children: <Widget>[
                                new Row(
                                  children: <Widget>[
                                    new Column(
                                      children: <Widget>[
                                        new Padding(
                                          padding: new EdgeInsets.only(top: 1.0),
                                          child: new SizedBox(
                                            height: 45.0,
                                            width: 45.0,
                                            child: Image.asset("assets/images/contract.png"),
                                          ),
                                        )
                                      ],
                                    ),
                                    new Expanded(
                                      child: new GestureDetector(
                                        child: new Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
//                                t i t l e
                                            new Padding(
                                              padding: new EdgeInsets.only(
                                                  left: 8.0, right: 8.0, bottom: 8.0, top: 8.0),
                                              child: new Text(new_data[index]['id'],
                                                style: new TextStyle(
                                                  fontSize: 18.0,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
//                                d e s c r i p t i o n
                                            new Padding(
                                              padding: new EdgeInsets.only(
                                                  left: 8.0, right: 4.0, bottom: 4.0),
                                              child: new Text(new_data[index]['title'],
                                                style: new TextStyle(
                                                  fontSize: 16.0,
                                                  fontWeight: FontWeight.normal
                                                ),
                                              ),
                                            ),
                                            new Padding(
                                              padding: new EdgeInsets.only(
                                                  left: 8.0, right: 8.0, bottom: 8.0, top: 1.0),
                                              child: new Text("Tarikh mula arahan kerja: " + new_data[index]['startDate'],
                                                style: new TextStyle(
                                                  color: Colors.grey[500],
                                                  fontWeight: FontWeight.normal,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        onTap: () {
                                          //go to details page
                                          Navigator.of(context).push(new MaterialPageRoute(
                                              builder: (BuildContext context) => new MyHero()));
                                        },
                                      ),
                                    ),
                                    new Column(
                                      children: <Widget>[
                                        new Padding(
                                            padding: EdgeInsets.only(left: 4.0),
                                        child: Icon(Icons.keyboard_arrow_right, color: Colors.blue, size: 30.0),)
                                      ],
                                    )
                                  ],
                                )
                              ],
                            )),
                      );
                    },

                  );
                }),
          ),
        ));
  }
}



