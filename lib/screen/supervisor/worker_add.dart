import 'package:cma_app/screen/supervisor/homepage.dart';
import 'package:cma_app/screen/supervisor/po_active.dart';
import 'package:cma_app/screen/supervisor/po_no_workplan.dart';
import 'package:cma_app/screen/supervisor/timetable.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../myDrawer.dart';

class WorkerAdd extends StatefulWidget {
  @override
  _WorkerAddState createState() => _WorkerAddState();
}

class _WorkerAddState extends State<WorkerAdd> {

  Widget _buildForm(){
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: <Widget>[
          Column(
            crossAxisAlignment:CrossAxisAlignment.start ,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 10.0,bottom: 8.0),
                child: Text('Nama Pekerja Anda'),
              ),
              Padding(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 10.0, top: 0),
                child: TextFormField(
                  style: TextStyle(
                    fontSize: 14.0,
                    color: Colors.black87,
                  ),
                  decoration: InputDecoration(
                    fillColor: Colors.white,
                    contentPadding:
                    EdgeInsets.symmetric(vertical: 20, horizontal: 20.0),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15.0),
                      borderSide: BorderSide(),
                    ),
                  ),
                  keyboardType: TextInputType.multiline,
                  maxLines: 2,
                ),
              ),
            ],
          ),
          Column(
            crossAxisAlignment:CrossAxisAlignment.start ,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 10.0,bottom: 8.0),
                child: Text('Nombor Telefon'),
              ),
              Padding(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 10.0, top: 0),
                child: TextFormField(
                  style: TextStyle(
                    fontSize: 14.0,
                    color: Colors.black87,
                  ),
                  decoration: InputDecoration(
                    fillColor: Colors.white,
                    contentPadding:
                    EdgeInsets.symmetric(vertical: 20, horizontal: 20.0),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15.0),
                      borderSide: BorderSide(),
                    ),
                  ),

                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildButton() {
    return Padding(
      padding: const EdgeInsets.all(18.0),
      child: SizedBox(
        width: double.infinity,
        child: RaisedButton.icon(
          onPressed: () {
            Navigator.of(context).pop();
            Navigator.of(context).push(MaterialPageRoute(
                builder: (BuildContext context) => Homepage()));
          },
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          label: Text(
            'Simpan',
            style: TextStyle(
                fontFamily: 'BellotaText',
                color: Colors.white,
                letterSpacing: 3),
          ),
          icon: Padding(
            padding: EdgeInsets.all(10.0),
            child: Icon(
              Icons.save,
            ),
          ),
          textColor: Colors.white,
          splashColor: Colors.red.shade900,
          color: Colors.red.shade600,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:  Text(
          "CMA Contractor",

        ),
        centerTitle: true,
        backgroundColor: Colors.blue.shade700,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () { Navigator.of(context).pop(); },

            );
          },
        ),
      ),
      drawer: MyDrawer(),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(18.0),
                  child: Text(
                    'Tambah Pekerja',
                    style: TextStyle(
                      fontSize: 17,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 1
                    ),
                  ),
                ),
                Icon(Icons.person_add, color: Colors.blue.shade700,),
              ],
            ),
            _buildForm(),
            _buildButton(),
          ],
        ),
      ),
    );
  }
}

