import 'package:cma_app/myDrawer.dart';
import 'package:cma_app/screen/supervisor/homepage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../utilities.dart';

class AddWorkplan extends StatefulWidget {
  @override
  _AddWorkplanState createState() => _AddWorkplanState();
}

class _AddWorkplanState extends State<AddWorkplan> {
  DateTime _dateTime;


  Widget _myform(BuildContext context) {

    return ListView(
      children: <Widget>[
        Card(

          elevation: 0,
          child:  Padding(
            padding:  EdgeInsets.all(10.0),
            child:  Column(
              children: <Widget>[
                 Row(
                  children: <Widget>[
                     Expanded(
                        child:  Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                             Padding(
                              padding:  EdgeInsets.only(
                                  left: 8.0, right: 8.0, bottom: 8.0, top: 8.0),
                              child:  Text(
                                "PO 1234567",
                                style:  TextStyle(
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                             Padding(
                              padding:  EdgeInsets.only(
                                  left: 8.0, right: 4.0, bottom: 4.0),
                              child:  Text(
                                'Kerja-kerja pembersihan dan selenggara am.',
                                style:  TextStyle(
                                    fontSize: 18.0,
                                    fontWeight: FontWeight.normal),
                              ),
                            ),
                             Padding(
                              padding:  EdgeInsets.only(
                                  left: 8.0, right: 8.0, bottom: 0, top: 10.0),
                              child:  Text(
                                "Tarikh mula arahan kerja: 10 Mar 2020",
                                style:  TextStyle(
                                  color: Colors.blueGrey[700],
                                  fontWeight: FontWeight.normal,
                                  fontSize: 16.0,
                                ),
                              ),
                            ),
                             Padding(
                              padding:  EdgeInsets.only(
                                  left: 8.0, right: 8.0, bottom: 10.0, top: 5.0),
                              child:  Text(
                                "Tarikh mula arahan kerja: 10 Apr 2020",
                                style:  TextStyle(
                                  color: Colors.blueGrey[700],
                                  fontWeight: FontWeight.normal,
                                  fontSize: 16.0,
                                ),
                              ),
                            ),
                            Divider(),
                            Padding(
                              padding:  EdgeInsets.only(
                                  left: 8.0, right: 8.0, bottom: 10.0, top: 5.0),
                              child:  TextFormField(
                                style: TextStyle(
                                  fontSize: 14.0,
                                  color: Colors.black87,
                                ),
                                decoration:  InputDecoration(
                                  labelText: "Perancangan Tarikh Mula Kerja:   ",
                                  fillColor: Colors.white,
                                  contentPadding:
                                   EdgeInsets.symmetric(
                                      vertical: 0,
                                      horizontal: 20.0),
                                  border:  OutlineInputBorder(
                                    borderRadius:
                                     BorderRadius.circular(15.0),
                                    borderSide:  BorderSide(),
                                  ),
                                  suffixIcon: GestureDetector(
                                    onTap: () {
                                      showDatePicker(
                                          context: context,
                                          initialDate: _dateTime == null ? DateTime.now() : _dateTime,
                                          firstDate: DateTime(2001),
                                          lastDate: DateTime(2021)
                                      ).then((date) {
                                        setState(() {
                                          _dateTime = date;
                                        });
                                      });
                                    },
                                    child: Icon( Icons.calendar_today),
                                  ),
                                ),
                              ),
                            ),
                            Divider(),
                            Padding(
                              padding:  EdgeInsets.only(
                                  left: 8.0, right: 8.0, bottom: 10.0, top: 0),
                              child:  TextFormField(
                                style: TextStyle(
                                  fontSize: 14.0,
                                  color: Colors.black87,
                                ),
                                decoration:  InputDecoration(
                                  labelText: "Maklumat Lain (Sekiranya Ada):   ",
                                  fillColor: Colors.white,
                                  contentPadding:
                                   EdgeInsets.symmetric(
                                      vertical: 20,
                                      horizontal: 20.0),
                                  border:  OutlineInputBorder(
                                    borderRadius:
                                     BorderRadius.circular(15.0),
                                    borderSide:  BorderSide(),
                                  ),
                                ),
                                keyboardType: TextInputType.multiline,
                                maxLines: 10,
                              ),
                            ),

                            
                          ],
                        ),
                    ),

                  ],
                ),
              ],
            ),
          ),
        ),
         Padding(
          padding: const EdgeInsets.all(20.0),
          child: SizedBox(
            width: double.infinity,
            child: RaisedButton.icon(
              onPressed: () {
                // Set intial mode to login
                Navigator.of(context).pop();
                Navigator.of(context).push( MaterialPageRoute(
                    builder: (BuildContext context) =>  Homepage()));
              },
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(20.0))),
              label: Text(
                'Hantar',
                style: TextStyle(
                    fontFamily: 'BellotaText',
                    color: Colors.white,
                    letterSpacing: 3),
              ),
              icon: Padding(
                padding: EdgeInsets.all(10.0),
                child: Icon(
                  Icons.file_upload,
                ),
              ),
              textColor: Colors.white,
              splashColor: Colors.red.shade900,
              color: Colors.red.shade600,
            ),
          ),
        )



      ],
    );
  }


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title:  Text(
          "Add New Workplan",
          style: TextStyle(
            letterSpacing: 1,
            fontFamily: "Lato",
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.blue.shade700,
      ),
      drawer: MyDrawer(),
      body: _myform(context),






    );
  }
}
