import 'dart:convert';

import 'package:cma_app/animation/fade_animation.dart';
import 'package:cma_app/screen/supervisor/homepage.dart';
import 'package:cma_app/screen/supervisor/po_active.dart';
import 'package:cma_app/screen/supervisor/po_no_workplan.dart';
import 'package:cma_app/screen/supervisor/timetable.dart';
import 'package:cma_app/screen/supervisor/worker_assign.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../myDrawer.dart';

class WorkerList extends StatefulWidget {
  @override
  _WorkerListState createState() => _WorkerListState();
}

class _WorkerListState extends State<WorkerList> {
  MediaQueryData queryData;


  Widget _phoneNo(data) {
    TextStyle _LocationTextStyle = TextStyle(
      fontFamily: "SourceSansPro",
      fontSize: 14,
      fontWeight: FontWeight.bold,
      color: Colors.black45,
    );

    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Icon(
          Icons.phone,
          color: Colors.green,
          size: 16.0,
        ),
        SizedBox(
          width: 5.0,
        ),
        Text(
          data,
          style: _LocationTextStyle,
        ),
      ],
    );
  }

  Widget _buildName(data) {
    return Row(
      children: <Widget>[
        Icon(
          Icons.account_circle,
          color: Colors.grey,
          size: 15,
        ),
        SizedBox(
          width: 5,
        ),
        Text(
          data["name"],
          style: (TextStyle(
              fontSize: 17,
              fontWeight: FontWeight.bold,
              letterSpacing: .5,
              fontFamily: "SourceSansPro")),
        ),
      ],
    );
  }

  Widget _bulildWorker(data) {
    queryData = MediaQuery.of(context);

    TextStyle _CheckInTextStyle = TextStyle(
      fontFamily: 'SourceSansPro',
      color: Colors.blueGrey,
      fontSize: 15.0,
    );
    TextStyle _kodTextStyle = TextStyle(
        fontFamily: 'SourceSansPro',
        color: Colors.blueGrey.shade800,
        fontSize: 15.0,
        fontWeight: FontWeight.w800,
        letterSpacing: .5);
    TextStyle _kodTextStyle2 = TextStyle(
        fontFamily: 'SourceSansPro',
        color: Colors.blueGrey.shade800,
        fontSize: 16.0,
        fontWeight: FontWeight.w400,
        letterSpacing: .5);

    return Expanded(
      child: Container(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  _buildName(data),
                ],
              ),
              _phoneNo(data["phone"]),
              Divider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Icon(
                    Icons.location_city,
                    color: Colors.blueGrey.shade400,
                  ),
                  SizedBox(width: 5.0),
                  Text("Kod Syarikat", style: (_CheckInTextStyle)),
                  SizedBox(width: 10.0),
                  Text(data["kodsyarikat"], style: (_kodTextStyle)),
                ],
              ),
              Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Icon(
                      Icons.person,
                      color: Colors.blueGrey.shade400,
                    ),
                    SizedBox(width: 5.0),
                    Text(
                      "Kod Pekerja",
                      style: (_CheckInTextStyle),
                    ),
                    SizedBox(width: 10.0),
                    Text(data["kodpekerja"], style: (_kodTextStyle)),
                  ]),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildButton() {
    return Padding(
      padding: const EdgeInsets.all(18.0),
      child: SizedBox(
        width: double.infinity,
        child: RaisedButton.icon(
          onPressed: () {
            Navigator.of(context).pop();
            Navigator.of(context).push(MaterialPageRoute(
                builder: (BuildContext context) => Homepage()));
          },
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          label: Text(
            'Simpan',
            style: TextStyle(
                fontFamily: 'BellotaText',
                color: Colors.white,
                letterSpacing: 3),
          ),
          icon: Padding(
            padding: EdgeInsets.all(10.0),
            child: Icon(
              Icons.save,
            ),
          ),
          textColor: Colors.white,
          splashColor: Colors.red.shade900,
          color: Colors.red.shade600,
        ),
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title:  Text(
            "CMA Contractor",

          ),
          centerTitle: true,
          backgroundColor: Colors.blue.shade700,
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: const Icon(Icons.arrow_back),
                onPressed: () { Navigator.of(context).pop(); },

              );
            },
          ),


        ),
        drawer: MyDrawer(),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(18.0),
                    child: Text(
                      'Senarai Pekerja',
                      style: TextStyle(
                          fontSize: 17,
                          fontWeight: FontWeight.bold,
                          letterSpacing: 1
                      ),
                    ),
                  ),
                  Icon(Icons.people, color: Colors.blue.shade700,),
                ],
              ),
              FutureBuilder(
                  future: DefaultAssetBundle.of(context)
                      .loadString('data/sampleWorker.json'),
                  builder: (context, snapshot) {
                    // Decode the JSON
                    var new_data = jsonDecode(snapshot.data.toString());
                    return ListView.builder(
                      shrinkWrap: true,
                      itemCount: new_data == null ? 0 : new_data.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          child: FadeAnimation(
                              1,
                              GestureDetector(
                                child: Card(
                                  elevation: .75,
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Row(
                                      children: <Widget>[
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Column(
                                              // align the text to the left instead of centered
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Row(
                                                  children: <Widget>[
                                                    Icon(FontAwesomeIcons.userAlt,color: Colors.black26,size: 15,),
                                                    Text(
                                                      new_data[index]['name'],
                                                      style: TextStyle(fontSize: 16),
                                                    ),
                                                  ],
                                                ),
                                                SizedBox(height: 5.0,),
                                                Row(
                                                  children: <Widget>[
                                                    Icon(Icons.phone,color: Colors.black26 ,size: 15,),
                                                    Text(new_data[index]['phone']),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),

                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Column(
                                              // align the text to the left instead of centered
                                              crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Row(
                                                  children: <Widget>[
                                                    Icon(FontAwesomeIcons.building,color: Colors.black26 ,size: 15,),
                                                    SizedBox(width: 3,),
                                                    Text(
                                                      'Kod Syarikat : '+
                                                      new_data[index]['kodsyarikat'],

                                                    ),
                                                  ],
                                                ),
                                                SizedBox(height: 5.0,),
                                                Row(
                                                  children: <Widget>[
                                                    Icon(FontAwesomeIcons.idBadge,color: Colors.black26 ,size: 15,),
                                                    SizedBox(width: 3,),
                                                    Text(
                                                      'Kod Pekerja : '+
                                                        new_data[index]['kodpekerja']),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                onTap: (){
                                  Navigator.of(context).pop();
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (BuildContext context) => Homepage()));
                                },
                              )),
                        );
                      },
                    );
                  }),
            ],
          ),
        ));
  }
}
