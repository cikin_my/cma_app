import 'dart:convert';

import 'package:cma_app/animation/fade_animation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../myDrawer.dart';

class WorkerList2 extends StatefulWidget {
  @override
  _WorkerList2State createState() => _WorkerList2State();
}

class _WorkerList2State extends State<WorkerList2> {

  MediaQueryData queryData;


  Widget _phoneNo(data) {
    TextStyle _LocationTextStyle = TextStyle(
      fontFamily: "SourceSansPro",
      fontSize: 14,
      fontWeight: FontWeight.bold,
      color: Colors.black45,
    );

    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Icon(
          Icons.phone,
          color: Colors.green,
          size: 16.0,
        ),
        SizedBox(
          width: 5.0,
        ),
        Text(
          data,
          style: _LocationTextStyle,
        ),
      ],
    );
  }

  Widget _buildName(data) {
    return Row(
      children: <Widget>[
        Icon(
          Icons.account_circle,
          color: Colors.grey,
          size: 15,
        ),
        SizedBox(
          width: 5,
        ),
        Text(
          data["name"],
          style: (TextStyle(
              fontSize: 17,
              fontWeight: FontWeight.bold,
              letterSpacing: .5,
              fontFamily: "SourceSansPro")),
        ),
      ],
    );
  }

  Widget _bulildWorker(data) {
    queryData = MediaQuery.of(context);


    TextStyle _CheckInTextStyle = TextStyle(
      fontFamily: 'SourceSansPro',
      color: Colors.blueGrey,
      fontSize: 15.0,

    );
    TextStyle _kodTextStyle = TextStyle(
      fontFamily: 'SourceSansPro',
      color: Colors.blueGrey.shade800,
      fontSize: 15.0,
      fontWeight: FontWeight.w800,
      letterSpacing: .5

    );
    TextStyle _kodTextStyle2 = TextStyle(
        fontFamily: 'SourceSansPro',
        color: Colors.blueGrey.shade800,
        fontSize: 16.0,
        fontWeight: FontWeight.w400,
        letterSpacing: .5

    );

    return Expanded(
      child: Container(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  _buildName(data),

                ],
              ),
              _phoneNo(data["phone"]),

              Divider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Icon(Icons.location_city, color: Colors.blueGrey.shade400,),
                  SizedBox(width:5.0),
                  Text("Kod Syarikat",style: (_CheckInTextStyle)),
                  SizedBox(width:10.0),
                  Text(data["kodsyarikat"], style: (_kodTextStyle)),
                ],
              ),

              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Icon(Icons.person, color: Colors.blueGrey.shade400,),
                  SizedBox(width:5.0),
                  Text("Kod Pekerja",style: (_CheckInTextStyle),),
                  SizedBox(width:10.0),
                  Text(data["kodpekerja"], style: (_kodTextStyle)),
          ]


              ),

            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "CMA Contractor"  ,
          ),
          centerTitle: true,

        ),
        drawer: MyDrawer(),
        body: SingleChildScrollView(
          child: Column(

            children: <Widget>[
              Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left:20.0,bottom: 10.0,top: 20.0),
                    child: Text("Senarai Pekerja",textAlign: TextAlign.left,),
                  ),
                ],
              ),
              FutureBuilder(
                  future: DefaultAssetBundle.of(context)
                      .loadString('data/sampleWorker.json'),
                  builder: (context, snapshot) {
                    // Decode the JSON
                    var new_data = jsonDecode(snapshot.data.toString());
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: new_data == null ? 0 : new_data.length,
                        // Build the ListView
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            child: FadeAnimation(
                              1,
                              Card(
                                elevation: 0.75,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.only(right:8.0),
                                      child: CircleAvatar(
                                        radius: 25,
                                        backgroundColor: Colors.transparent,
                                        backgroundImage: AssetImage('assets/images/worker.png'),
                                      ),
                                    ),
                                    Expanded(
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Column(

                                                  children: <Widget>[
                                                    Text(new_data[index]['name'], style: TextStyle(fontSize: 16,fontWeight: FontWeight.w500),),
                                                    Row(
                                                      children: <Widget>[
                                                        Icon(Icons.phone, color: Colors.green, size: 16,),
                                                        Text(new_data[index]['phone']),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                                Divider(),
                                                Row(
                                                  children: <Widget>[
                                                    Icon(Icons.location_city, color: Colors.blueGrey, size: 16,),
                                                    Text('Kod Syarikat : '),
                                                    Text(new_data[index]['kodsyarikat']),
                                                  ],
                                                ),
                                                SizedBox(height: 5.0,),
                                                Row(
                                                  children: <Widget>[
                                                    Icon(Icons.account_circle, color: Colors.blueGrey, size: 16,),
                                                    Text('Kod Pekerja : '),
                                                    Text(new_data[index]['kodpekerja']),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),

                                        ],
                                      ),
                                    ),
                                  ],
                                )
                              ),
                            ),
                          );
                        },
                      ),
                    );
                  }),
            ],
          ),
        ));
  }
}
