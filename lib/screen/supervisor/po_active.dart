import 'dart:convert';

import 'package:cma_app/screen/supervisor/homepage.dart';
import 'package:cma_app/screen/supervisor/worker_assign.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class POActive extends StatefulWidget {
  @override
  _POActiveState createState() => _POActiveState();
}

class _POActiveState extends State<POActive> {
  Widget _buildContent(_data) {
    return InkWell(
      onTap: (){
        Navigator.of(context).push(MaterialPageRoute(
            builder: (BuildContext context) =>
                WorkerAssign()));
      },
      highlightColor: Colors.white30,
      splashColor: Colors.blue.shade50,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: <Widget>[
                      Image.asset(
                        'assets/images/activepo.png',
                        height: 30,
                        width: 30,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          _data['id'],
                          style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold,
                              letterSpacing: 1),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 8, 8, 2),
                    child: Text(
                      _data['title'],
                      style: TextStyle(
                          fontSize: 15.0, fontWeight: FontWeight.w500),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 0, 8, 8),
                    child: Text(
                      "Tarikh mula arahan kerja: " + _data['startDate'],
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.grey.shade700,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.only(left: 4.0),
                    child: GestureDetector(
                      child: Icon(Icons.keyboard_arrow_right,
                          color: Colors.blue, size: 30.0),
                      onTap: () {
                        //go to PO  Work Plan page
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) =>
                                WorkerAssign()));
                      },
                    )),
              ],
            )
          ],
        ),
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          child: Center(
            child: FutureBuilder(
                future: DefaultAssetBundle.of(context)
                    .loadString('data/mydata.json'),
                builder: (context, snapshot) {
                  // Decode the JSON
                  var _data = jsonDecode(snapshot.data.toString());

                  return ListView.builder(
                    itemCount: _data == null ? 0 : _data.length,
                    // Build the ListView
                    itemBuilder: (BuildContext context, int index) {
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Card(
                          elevation: 1,
                          child: Padding(
                              padding: EdgeInsets.all(10.0),
                              child: Column(
                                children: <Widget>[
                                  _buildContent(_data[index]),
                                ],
                              )),
                        ),
                      );
                    },
                  );
                }),
          ),
        ));
  }
}
