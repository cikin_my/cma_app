

import 'package:cma_app/screen/supervisor/po_active.dart';
import 'package:cma_app/screen/supervisor/po_no_workplan.dart';
import 'package:cma_app/screen/supervisor/readjson.dart';
import 'package:cma_app/screen/supervisor/timetable.dart';
import 'package:cma_app/screen/supervisor/timetable_supervisor.dart';
import 'package:cma_app/screen/supervisor/worker_add.dart';
import 'package:cma_app/screen/supervisor/worker_list.dart';
import 'package:cma_app/myDrawer.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';


class Homepage extends StatefulWidget {
  @override
  _HomepageState createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  int _currentIndex = 0;
  final List<Widget> _children = [
    TimetableSupervisor(),
    POActive(),
    PO_NoWorkplan(),
  ];

  void onTappedBar(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      body: _children[_currentIndex],
      appBar: AppBar(
        title:  Text(
          "CMA Contractor",
          style: TextStyle(
            letterSpacing: 1,
            fontFamily: "Lato",
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.blue.shade700,
      ),
      drawer: MyDrawer(),
      bottomNavigationBar: BottomNavigationBar(
          onTap: onTappedBar,
          currentIndex: _currentIndex,
          items: [
            BottomNavigationBarItem(
              icon:  Icon(Icons.calendar_today),
              title:  Text('Jadual Kerja',style: TextStyle(letterSpacing: .75),),
            ),
            BottomNavigationBarItem(
              icon:  Icon(FontAwesomeIcons.tools),
              title:  Text('workplan',style: TextStyle(letterSpacing: .75),),
            ),
            BottomNavigationBarItem(
              icon:  Icon(FontAwesomeIcons.exclamation),
              title:  Text('No Workplan',style: TextStyle(letterSpacing: .75),),
            ),

          ]),
    );
  }
}

