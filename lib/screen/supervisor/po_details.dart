import 'package:cma_app/myDrawer.dart';
import 'package:cma_app/screen/supervisor/workplan_new.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class PoDetails extends StatelessWidget {
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            /*1*/
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                /*2*/
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text(
                    'PO 454545445',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Text(
                  'Kerja-kerja pembersihan dan selenggara am.',
                  style: TextStyle(
                    color: Colors.grey[500],
                  ),
                ),
              ],
            ),
          ),
          /*3*/
          Icon(
            Icons.star,
            color: Colors.red[500],
          ),
        ],
      ),
    );
    Widget textSection = Container(
        padding: const EdgeInsets.all(32),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Row(
              children: <Widget>[
                FaIcon(
                  FontAwesomeIcons.calendarDay,
                  size: 18.0,
                ),
                Text(' Tarikh mula arahan kerja : '),
                Text(
                  '10 Apr 2020',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    letterSpacing: 2,
                    color: Colors.black,
                  ),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                FaIcon(
                  FontAwesomeIcons.solidCalendarCheck,
                  size: 18.0,
                ),
                Container(
                  child: Text(' Tarikh luput arahan kerja : '),
                ),
                Text(
                  '10 Dec 2020',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    letterSpacing: 2,
                    color: Colors.blue.shade900,
                  ),
                ),
              ],
            ),
          ],
        ));
    Widget buttonSection = Container(
        padding: const EdgeInsets.all(32),
        child: SizedBox(
          width: double.infinity,
          child: RaisedButton.icon(
            onPressed: () {
              // Set intial mode to login
              Navigator.of(context).pop();
              Navigator.of(context).push(new MaterialPageRoute(
                  builder: (BuildContext context) =>  AddWorkplan()));
            },
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(20.0))),
            label: Text(
              'Rancang Kerja (Work Plan)',
              style: TextStyle(
                  fontFamily: 'BellotaText',
                  color: Colors.white,
                  letterSpacing: 3),
            ),
            icon: Padding(
              padding: EdgeInsets.all(10.0),
              child: Icon(
                Icons.edit,
              ),
            ),
            textColor: Colors.white,
            splashColor: Colors.red.shade900,
            color: Colors.red.shade600,
          ),
        ));

    return Scaffold(
      appBar: AppBar(
        title:  Text(
          "PO Details",
          style: TextStyle(
            letterSpacing: 1,
            fontFamily: "Lato",
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.blue.shade700,
      ),
      drawer: MyDrawer(),
      body: ListView(
        children: [
          Image.asset(
            'assets/images/login.jpg',
            width: 600,
            height: 240,
            fit: BoxFit.cover,
          ),
          titleSection,
          textSection,
          buttonSection,
        ],
      ),
    );
  }
}
