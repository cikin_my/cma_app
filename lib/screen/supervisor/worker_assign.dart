import 'package:cma_app/screen/supervisor/homepage.dart';
import 'package:cma_app/screen/supervisor/po_active.dart';
import 'package:cma_app/screen/supervisor/work_complete.dart';
import 'package:cma_app/screen/supervisor/worker_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../myDrawer.dart';

class WorkerAssign extends StatefulWidget {
  @override
  _WorkerAssignState createState() => _WorkerAssignState();
}

class _WorkerAssignState extends State<WorkerAssign> {
  Widget _buildInfoLine1() {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(18.0),
        child: Column(
          children: <Widget>[
//            Chip(
//              backgroundColor: Colors.green,
//              label: Text(
//                'Langkah 2',
//                style: TextStyle(
//                    letterSpacing: 1, color: Colors.white, fontSize: 12),
//              ),
//            ),
            Text(
              "Pilih salah seorang pekerja anda dan beri tugasan ",
              style: TextStyle(
                color: Colors.blueGrey,
                fontWeight: FontWeight.normal,
                letterSpacing: .5,
              ),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildInfoLine2() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "PO 1234567",
            style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: 16,
            ),
          ),
          SizedBox(
            height: 3.0,
          ),
          Text(
            'Kerja-kerja pembersihan dan selenggara am.',
            style: (TextStyle(fontSize: 16)),
          ),
          SizedBox(
            height: 18.0,
          ),
          Text(
            "Tarikh mula arahan kerja: 10 Mar 2020",
            style: TextStyle(
              color: Colors.black,
              letterSpacing: .7,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildForm() {
    return Column(
      children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 10.0, bottom: 8.0),
              child: Text('Pekerja Anda'),
            ),
            Padding(
              padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 10.0),
              child: TextFormField(
                style: TextStyle(
                  fontSize: 14.0,
                  color: Colors.black87,
                ),
                decoration: InputDecoration(
                  fillColor: Colors.white,
                  contentPadding:
                      EdgeInsets.symmetric(vertical: 0, horizontal: 20.0),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide: BorderSide(),
                  ),
                  suffixIcon: GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                      Navigator.of(context).push(
                        MaterialPageRoute(
                            builder: (BuildContext context) => WorkerList()),
                      );
                    },
                    child: Icon(
                      Icons.person_add,
                      color: Colors.red,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 10.0, bottom: 8.0),
              child: Text('Kerja yang perlu disiapkan'),
            ),
            Padding(
              padding:
                  EdgeInsets.only(left: 8.0, right: 8.0, bottom: 10.0, top: 0),
              child: TextFormField(
                style: TextStyle(
                  fontSize: 14.0,
                  color: Colors.black87,
                ),
                decoration: InputDecoration(
                  fillColor: Colors.white,
                  contentPadding:
                      EdgeInsets.symmetric(vertical: 20, horizontal: 20.0),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide: BorderSide(),
                  ),
                ),
                keyboardType: TextInputType.multiline,
                maxLines: 2,
              ),
            ),
          ],
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 10.0, bottom: 8.0),
              child: Text('Lokasi'),
            ),
            Padding(
              padding:
                  EdgeInsets.only(left: 8.0, right: 8.0, bottom: 10.0, top: 0),
              child: TextFormField(
                style: TextStyle(
                  fontSize: 14.0,
                  color: Colors.black87,
                ),
                decoration: InputDecoration(
                  fillColor: Colors.white,
                  contentPadding:
                      EdgeInsets.symmetric(vertical: 20, horizontal: 20.0),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide: BorderSide(),
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget _buildButton() {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: SizedBox(
        width: double.infinity,
        child: RaisedButton.icon(
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (BuildContext context) => POActive()));
          },
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          label: Text(
            'Hantar',
            style: TextStyle(
                fontFamily: 'BellotaText',
                color: Colors.white,
                letterSpacing: 3),
          ),
          icon: Padding(
            padding: EdgeInsets.all(10.0),
            child: Icon(
              Icons.file_upload,
            ),
          ),
          textColor: Colors.white,
          splashColor: Colors.blue.shade900,
          color: Colors.blue.shade600,
        ),
      ),
    );
  }

  Widget _buildButtonClose() {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: SizedBox(
        width: double.infinity,
        child: RaisedButton.icon(
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (BuildContext context) => WorkComplete()));
          },
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          label: Text(
            'Workplan Selesai',
            style: TextStyle(
                fontFamily: 'BellotaText',
                color: Colors.white,
                letterSpacing: 3),
          ),
          icon: Padding(
            padding: EdgeInsets.all(10.0),
            child: Icon(
              Icons.done,
            ),
          ),
          textColor: Colors.white,
          splashColor: Colors.red.shade900,
          color: Colors.red.shade600,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:  Text(
          "Assign Worker",
          style: TextStyle(
            letterSpacing: 1,
            fontFamily: "Lato",
          ),
        ),
        backgroundColor: Colors.blue.shade700,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (BuildContext context) => POActive()));
          },
        ),
        centerTitle: true,
        // backgroundColor: Colors.transparent,
        //elevation: 0,
        //iconTheme: IconThemeData(color: Colors.blueAccent.shade700),
      ),
      drawer: MyDrawer(),
      body: ListView(
        children: <Widget>[
          Padding(
              padding: EdgeInsets.all(10.0),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Column(
                          children: <Widget>[
                            _buildInfoLine1(),
                            SizedBox(
                              height: 20.0,
                            ),
                            _buildInfoLine2(),
                            Divider(),
                            _buildForm(),
                            _buildButton(),
                            Column(children: <Widget>[
                              Row(children: <Widget>[
                                Expanded(
                                  child: new Container(
                                      margin: const EdgeInsets.only(
                                          left: 10.0, right: 20.0),
                                      child: Divider(
                                        color: Colors.black,
                                        height: 36,
                                      )),
                                ),
                                Text("OR"),
                                Expanded(
                                  child: new Container(
                                      margin: const EdgeInsets.only(
                                          left: 20.0, right: 10.0),
                                      child: Divider(
                                        color: Colors.black,
                                        height: 36,
                                      )),
                                ),
                              ]),
//                              Text(
//                                'Mark this Workplan as finish and click button below to close this work plan.',
//                                style: TextStyle(
//                                    fontFamily: "BellotaText",
//                                    fontSize: 16,
//                                    fontStyle: FontStyle.italic),
//                                textAlign: TextAlign.center,
//                              ),
                              _buildButtonClose(),
                            ])
                          ],
                          crossAxisAlignment: CrossAxisAlignment.start,
                        ),
                      )
                    ],
                  )
                ],
              ))
        ],
      ),
    );
  }
}
