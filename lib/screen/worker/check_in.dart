
import 'package:cma_app/screen/supervisor/homepage.dart';
import 'package:cma_app/screen/worker/assignment_details.dart';
import 'package:cma_app/screen/worker/blocker_report.dart';
import 'package:cma_app/screen/worker/hazard.dart';
import 'package:cma_app/screen/worker/sqe_assessment.dart';
import 'package:cma_app/screen/worker/update_sqe.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../../myDrawer_worker.dart';

class CheckIn extends StatefulWidget {
  @override
  _CheckInState createState() => _CheckInState();
}

class _CheckInState extends State<CheckIn> {
  bool isCheckIn = false;
  bool isSqeAssessmentDone = false;

  void _showCheckOutButton() {
    setState(() {
      isCheckIn = true;
      isSqeAssessmentDone = true;
    });
  }

  Widget _titleSection() {
    return Container(
      padding: const EdgeInsets.fromLTRB(32, 32, 32, 8),
      child: Row(
        children: [
          Expanded(
            /*1*/
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                /*2*/
                Chip(
                  backgroundColor: Colors.green,
                  label: Text(
                    'PO 454545445',
                    style: TextStyle(
                      fontFamily: "Lato",
                      color: Colors.white,
                    ),
                  ),
                ),
                Text(
                  'Kerja-kerja pembersihan dan selenggara am.',
                  style: TextStyle(
                      color: Colors.grey[800],
                      fontFamily: "Lato",
                      fontSize: 16),
                ),
              ],
            ),
          ),
          /*3*/
          Icon(
            Icons.star,
            color: Colors.red[500],
          ),
        ],
      ),
    );
  }

  Widget _textSection() {
    return Container(
        padding: const EdgeInsets.all(32),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            ListTile(
              contentPadding: EdgeInsets.all(0.0),
              dense: true,
              leading: Icon(Icons.calendar_today),
              title: Text('Tarikh mula arahan kerja (Kontrak PO)'),
              subtitle: Text('10 April 2020'),
            ),
            ListTile(
              contentPadding: EdgeInsets.all(0.0),
              dense: true,
              leading: Icon(FontAwesomeIcons.calendarCheck),
              title: Text('Delivery Date (Kontrak PO)'),
              subtitle: Text('10 April 2020'),
            ),
            ListTile(
              contentPadding: EdgeInsets.all(0.0),
              dense: true,
              leading: Icon(Icons.business_center),
              title: Text('Vendor '),
              subtitle: Text('Bina Maju Sdn. Bhd.'),
            ),
            ListTile(
              contentPadding: EdgeInsets.all(0.0),
              dense: true,
              leading: Icon(FontAwesomeIcons.calendarTimes),
              title: Text('Tarikh Rancang Mula Kerja (Workplan)'),
              subtitle: Text('15 Mar 2020'),
            ),
            ListTile(
              contentPadding: EdgeInsets.all(0.0),
              dense: true,
              leading: Icon(Icons.info_outline),
              title: Text('Maklumat Lain'),
              subtitle: Text('Limited Access due to MCO.'),
            ),
            ListTile(
              contentPadding: EdgeInsets.all(0.0),
              dense: true,
              leading: Icon(Icons.today),
              title: Text('Tarikh Penugasan:'),
              subtitle: Text('10 April 2020'),
            ),
            ListTile(
              contentPadding: EdgeInsets.all(0.0),
              dense: true,
              leading: Icon(FontAwesomeIcons.userCircle),
              title: Text('Nama Staff'),
              subtitle: Text('Samad'),
            ),
            ListTile(
              contentPadding: EdgeInsets.all(0.0),
              dense: true,
              leading: Icon(Icons.input),
              title: Text('Tarikh Check In: '),
              subtitle: Text('30 April 2020'),
            ),
          ],
        ));
  }

  Widget _buttonCheckin() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(32, 5, 32, 8),
      child: SizedBox(
        width: double.infinity,
        child: RaisedButton.icon(
          onPressed: () {
            setState(() {
              isCheckIn = true;
            });
          },
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          label: Text(
            'Check In',
            style: TextStyle(
                fontFamily: 'Lato',
                color: Colors.white,
                letterSpacing: 3),
          ),
          icon: Padding(
            padding: EdgeInsets.all(10.0),
            child: Icon(
              Icons.input,
            ),
          ),
          textColor: Colors.white,
          splashColor: Colors.blue.shade900,
          color: Colors.blue.shade600,
        ),
      ),
    );
  }

  Widget _buttonCheckout() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(32, 5, 32, 8),
      child: SizedBox(
        width: double.infinity,
        child: RaisedButton.icon(
          onPressed: () {},
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          label: Text(
            'Check Out',
            style: TextStyle(
                fontFamily: 'Lato',
                color: Colors.white,
                letterSpacing: 3),
          ),
          icon: Padding(
            padding: EdgeInsets.all(10.0),
            child: Icon(
              Icons.check,
            ),
          ),
          textColor: Colors.white,
          splashColor: Colors.green.shade900,
          color: Colors.green.shade600,
        ),
      ),
    );
  }

  Widget _buttonSQEAssessment() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(32, 5, 32, 8),
      child: SizedBox(
        width: double.infinity,
        child: RaisedButton.icon(
          onPressed: () {
            Navigator.of(context).pop();
            Navigator.of(context).push(new MaterialPageRoute(
                builder: (BuildContext context) => SqeAssessment()));
          },
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          label: Text(
            'SQA Assessment',
            style: TextStyle(
                fontFamily: 'Lato',
                color: Colors.white,
                letterSpacing: 3),
          ),
          icon: Padding(
            padding: EdgeInsets.all(10.0),
            child: Icon(
              Icons.playlist_add_check,
            ),
          ),
          textColor: Colors.white,
          splashColor: Colors.black,
          color: Colors.blue.shade700,
        ),
      ),
    );
  }

  Widget _buttonReportBlocker() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(32, 5, 32, 8),
      child: SizedBox(
        width: double.infinity,
        child: RaisedButton.icon(
          onPressed: () {
            Navigator.of(context).pop();
            Navigator.of(context).push(new MaterialPageRoute(
                builder: (BuildContext context) => BlockerReport()));
          },
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          label: Text(
            'Report Blocker',
            style: TextStyle(
                fontFamily: 'Lato',
                color: Colors.white,
                letterSpacing: 3),
          ),
          icon: Padding(
            padding: EdgeInsets.all(10.0),
            child: Icon(
              Icons.report,
            ),
          ),
          textColor: Colors.white,
          splashColor: Colors.black,
          color: Colors.blue.shade700,
        ),
      ),
    );
  }

  Widget _buttonHazard() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(32, 5, 32, 8),
      child: SizedBox(
        width: double.infinity,
        child: RaisedButton.icon(
          onPressed: () {
            Navigator.of(context).pop();
            Navigator.of(context).push(new MaterialPageRoute(
                builder: (BuildContext context) => AddHazard()));
          },
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          label: Text(
            'Hazard ',
            style: TextStyle(
                fontFamily: 'Lato',
                color: Colors.white,
                letterSpacing: 3),
          ),
          icon: Padding(
            padding: EdgeInsets.all(10.0),
            child: Icon(
              FontAwesomeIcons.skullCrossbones,
            ),
          ),
          textColor: Colors.white,
          splashColor: Colors.red.shade900,
          color: Colors.red.shade600,
        ),
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Check In",
          style: TextStyle(
            letterSpacing: 1,
            fontFamily: "Lato",
          ),
        ),
        backgroundColor: Colors.blue.shade700,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context).push( MaterialPageRoute(
                builder: (BuildContext context) =>  Homepage()));
          },
        ),
        centerTitle: true,
      ),
      drawer: MyWorkerDrawer(),
      body: ListView(
        children: [
          _titleSection(),
          _textSection(),
          Visibility(
              visible: isCheckIn == true,
              child: Column(
                children: <Widget>[
                  _buttonSQEAssessment(),
                  _buttonReportBlocker(),
                  _buttonHazard(),
                ],
              )),
          Visibility(visible: isCheckIn == false, child: _buttonCheckin()),
          Visibility(
            visible: isCheckIn == true,
            child: _buttonCheckout(),
          ),
        ],
      ),
    );
  }
}
