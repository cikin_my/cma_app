  import 'dart:convert';
  import 'dart:ui';

  import 'package:cma_app/screen/worker/check_in.dart';
  import 'package:flutter/cupertino.dart';
  import 'package:flutter/material.dart';
  import 'package:flutter/painting.dart';
  import 'package:http/http.dart' as http;

  class AddHazard extends StatefulWidget {
    @override
    _AddHazardState createState() => _AddHazardState();
  }

  class _AddHazardState extends State<AddHazard> {
    Users _currentUser;

    final String uri =
        'https://bitbucket.org/cikin_my/myjson/raw/3fc8a93f3814df493a03a6e190df4b448434cfc8/hazard_wth_cat.json';

    Future<List<Users>> _fetchUsers() async {
      var response = await http.get(uri);
      var jsonData = json.decode(response.body);
      List<Users> hazardType = [];

      //if (response.statusCode == 200) {
      final items = json.decode(response.body).cast<Map<String, dynamic>>();
      List<Users> listOfUsers = items.map<Users>((json) {
        return Users.fromJson(json);
      }).toList();

      print(hazardType);
      return listOfUsers;

    }




    @override
    Widget build(BuildContext context) {
      return Scaffold(
        appBar: AppBar(
          title: Text(
            "Hazard & Langkah Kawalan",
            style: TextStyle(
              letterSpacing: 1,
              fontFamily: 'Lato',
            ),
          ),
          elevation: 0,
          centerTitle: true,
          backgroundColor: Colors.blue.shade700,
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: const Icon(Icons.arrow_back),
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                        builder: (BuildContext context) => CheckIn()),
                  );
                },
              );
            },
          ),
        ),
        body: ListView(
          children: <Widget>[

            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.more_vert,
                    color: Colors.red,
                  ),
                  Text(
                    "PENGENALPASTIAN HAZARD  & LANGKAH KAWALAN ",
                    style: TextStyle(
                        fontFamily: 'Lato', color: Colors.blueGrey.shade700),
                  ),
                ],
              ),
            ),
            Divider(),
            Padding(
              padding: const EdgeInsets.all(18.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  FutureBuilder<List<Users>>(
                      future: _fetchUsers(),
                      builder: (BuildContext context,
                          AsyncSnapshot<List<Users>> snapshot) {
                        if (!snapshot.hasData) return CircularProgressIndicator();
                        return Container(
                          child: DropdownButton<Users>(
                            items: snapshot.data
                                .map((user) => DropdownMenuItem<Users>(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  ListTile(
                                    title:Text(user.Kod_hazard + "-" + user.jenis_hazard),
                                    contentPadding: EdgeInsets.symmetric(horizontal: 0.0),
                                  ),
                                  Divider(),
                                ],
                              ),
                              value: user,
                            ))
                                .toList(),
                            onChanged: (Users value) {
                              setState(() {
                                _currentUser = value;
                              });
                            },
                            iconSize: 30,
                            icon: Icon(Icons.keyboard_arrow_down, color: Colors.blue.shade700,),
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 16,
                            ),
                            isExpanded: true,
                            //value: _currentUser,
                            hint: Text('Pilih Hazard'),
                          ),
                          padding: EdgeInsets.all(8.0),
                        );
                      }),
                  SizedBox(height: 20.0),
                  _currentUser != null
                      ? Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      ListTile(
                        title: Row(
                          children: <Widget>[
                            Text(_currentUser.Kod_hazard),
                            SizedBox(
                              width: 5.0,
                            ),
                            Expanded(child: Text(_currentUser.jenis_hazard)),
                          ],
                          crossAxisAlignment: CrossAxisAlignment.start,
                        ),
                        subtitle: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Expanded(
                                child: Text(_currentUser.kod_kawalan +"-" + _currentUser.langkah_kawalan,
                                  style: TextStyle(fontStyle: FontStyle.italic),
                                )),
                          ],
                        ),
                      ),
                    ],
                  )
                      : Text("Sila pilih Hazard"),
                ],
              ),
            ),
            Divider(),







          ],
        ),


      );
    }


  }

  class Users {
    int id;
    String Kod_hazard;
    String jenis_hazard;
    String langkah_kawalan;
    String kod_kawalan;

    Users({
      this.id,
      this.Kod_hazard,
      this.jenis_hazard,
      this.langkah_kawalan,
      this.kod_kawalan,
    });

    factory Users.fromJson(Map<String, dynamic> json) {
      return Users(
        id: json['id'],
        Kod_hazard: json['Kod_hazard'],
        jenis_hazard: json['jenis_hazard'],
        kod_kawalan: json['kod_kawalan'],
        langkah_kawalan: json['langkah_kawalan'],
      );
    }
  }