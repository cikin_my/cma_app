import 'dart:io';
import 'package:cma_app/model/ImageUploadModel.dart';
import 'package:cma_app/screen/worker/check_in.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

import '../../myDrawer_worker.dart';

class KemaskiniGambar extends StatefulWidget {
  @override
  _KemaskiniGambarState createState() => _KemaskiniGambarState();
}

class _KemaskiniGambarState extends State<KemaskiniGambar> {
  List<Object> images = List<Object>();
  Future<File> _imageFile;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      images.add("Add Image");
      images.add("Add Image");
      images.add("Add Image");
      images.add("Add Image");
      images.add("Add Image");
      images.add("Add Image");
      images.add("Add Image");
      images.add("Add Image");
      images.add("Add Image");
      images.add("Add Image");

    });
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new Scaffold(
        appBar: AppBar(
          title: Text(
            "Kemaskini Gambar ",
            style: TextStyle(
              letterSpacing: 1,
              fontFamily: "Lato",
            ),
          ),
          backgroundColor: Colors.blue.shade700,
          leading: new IconButton(
            icon: new Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).push(new MaterialPageRoute(
                  builder: (BuildContext context) => CheckIn()));
            },
          ),
          centerTitle: true,
        ),
        drawer: MyWorkerDrawer(),
        body:Padding(
          padding: const EdgeInsets.all(18.0),
          child: Column(
            children: <Widget>[
              Text("Kemaskini Gambar sebelum dan selepas ", style: (TextStyle(
                fontSize: 17,
                color: Colors.black87,
              )),),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Expanded(
                      child: Text("Sebelum", style: (TextStyle(
                        fontSize: 17,
                        color: Colors.black87,
                        fontFamily: "Lato",
                        letterSpacing:1,
                      )),
                        textAlign: TextAlign.center,),
                    ),
                    Expanded(
                      child: Text("Selepas", style: (TextStyle(
                        fontSize: 17,
                        color: Colors.black87,
                        fontFamily: "Lato",
                        letterSpacing:1,
                      )),
                        textAlign: TextAlign.center,),
                    ),
                  ],
                ),
              ),

              Expanded(
                child: buildGridView(),
              ),
            ],
          ),
        ),

      ),
    );
  }

  Widget buildGridView() {
    return GridView.count(
      shrinkWrap: true,
      crossAxisCount: 2,
      childAspectRatio: 1,
      children: List.generate(images.length, (index) {
        if (images[index] is ImageUploadModel) {
          ImageUploadModel uploadModel = images[index];
          return Card(
            clipBehavior: Clip.antiAlias,
            child: Stack(
              children: <Widget>[
                Image.file(
                  uploadModel.imageFile,
                  width: 300,
                  height: 300,
                ),
                Positioned(
                  right: 5,
                  top: 5,
                  child: InkWell(
                    child: Icon(
                      Icons.remove_circle,
                      size: 20,
                      color: Colors.red,
                    ),
                    onTap: () {
                      setState(() {
                        images.replaceRange(index, index + 1, ['Add Image']);
                      });
                    },
                  ),
                ),
              ],
            ),
          );
        } else {
          return Card(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                IconButton(
                  icon: Icon(Icons.camera_enhance, color: Colors.black26, size: 40,),
                  onPressed: () {
                    _onAddImageClick(index);
                  },
                ),
                SizedBox(height:10),
                Text("Add Picture",style: TextStyle(color: Colors.grey,letterSpacing: 1.5),),
              ],
            ),
          );
        }
      }),
    );
  }

  Future _onAddImageClick(int index) async {

    setState(() {
      _imageFile = ImagePicker.pickImage(source: ImageSource.camera);
      getFileImage(index);
    });
  }
  void getFileImage(int index) async {
//    var dir = await path_provider.getTemporaryDirectory();

    _imageFile.then((file) async {
      setState(() {
        ImageUploadModel imageUpload = new ImageUploadModel();
        imageUpload.isUploaded = false;
        imageUpload.uploading = false;
        imageUpload.imageFile = file;
        imageUpload.imageUrl = '';
        images.replaceRange(index, index + 1, [imageUpload]);
      });
    });
  }

}
