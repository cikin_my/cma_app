import 'package:cma_app/myDrawer_worker.dart';
import 'package:cma_app/screen/worker/check_in.dart';
import 'package:cma_app/screen/worker/homepage.dart';
import 'package:cma_app/screen/worker/update_sqe.dart';
import 'package:flutter/material.dart';


class BlockerReport extends StatefulWidget {
  @override
  _BlockerReportState createState() => _BlockerReportState();
}

class _BlockerReportState extends State<BlockerReport> {
  Widget _myform(BuildContext context) {
    return ListView(
      children: <Widget>[
        Card(

          elevation: 0,
          child: Padding(
            padding: EdgeInsets.all(10.0),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(
                                left: 8.0, right: 8.0, bottom: 8.0, top: 8.0),
                            child: Text(
                              "PO 1234567",
                              style: TextStyle(
                                fontSize: 18.0,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                left: 8.0, right: 4.0, bottom: 4.0),
                            child: Text(
                              'Kerja-kerja pembersihan dan selenggara am.',
                              style: TextStyle(
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.normal),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                left: 8.0, right: 8.0, bottom: 0, top: 10.0),
                            child: Text(
                              "Tarikh mula arahan kerja: 10 Mar 2020",
                              style: TextStyle(
                                color: Colors.grey[500],
                                fontWeight: FontWeight.normal,
                                fontSize: 16.0,
                              ),
                            ),
                          ),


                          Divider(),
                          Padding(
                            padding: EdgeInsets.only(
                                left: 8.0, right: 8.0, bottom: 10.0, top: 0),
                            child: TextFormField(
                              style: TextStyle(
                                fontSize: 16.0,
                                color: Colors.black87,
                              ),
                              decoration: InputDecoration(
                                labelText: "Masukan Blocker disini   ",
                                fillColor: Colors.white,
                                contentPadding:
                                EdgeInsets.symmetric(
                                    vertical: 20,
                                    horizontal: 20.0),
                                border: OutlineInputBorder(
                                  borderRadius:
                                  BorderRadius.circular(15.0),
                                  borderSide: BorderSide(),
                                ),


                              ),
                              keyboardType: TextInputType.multiline,
                              maxLines: 10,
                            ),
                          ),


                        ],
                      ),
                    ),

                  ],
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(20.0),
          child: SizedBox(
            width: double.infinity,
            child: RaisedButton.icon(
              onPressed: () {
                // Set intial mode to login
                Navigator.of(context).pop();
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => CheckIn()));
              },
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(20.0))),
              label: Text(
                'Hantar',
                style: TextStyle(
                    fontFamily: 'BellotaText',
                    color: Colors.white,
                    letterSpacing: 3),
              ),
              icon: Padding(
                padding: EdgeInsets.all(10.0),
                child: Icon(
                  Icons.file_upload,
                ),
              ),
              textColor: Colors.white,
              splashColor: Colors.blue.shade900,
              color: Colors.blue.shade700,
            ),
          ),
        )


      ],
    );
  }


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Report Blocker",
          style: TextStyle(
            letterSpacing: 1,
            fontFamily: "Lato",
          ),
        ),
        backgroundColor: Colors.blue.shade700,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context).push(new MaterialPageRoute(
                builder: (BuildContext context) => CheckIn()));
          },
        ),
        centerTitle: true,
      ),
      drawer: MyWorkerDrawer(),
      body: _myform(context),


    );
  }
}