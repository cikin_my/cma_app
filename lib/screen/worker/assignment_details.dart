import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:table_calendar/table_calendar.dart';
import 'dart:convert';

import '../../myDrawer.dart';

class AssignmentDetails extends StatefulWidget {
  @override
  _AssignmentDetailsState createState() => _AssignmentDetailsState();
}

class _AssignmentDetailsState extends State<AssignmentDetails> {
  Widget _buildInfoLine2() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "PO 1234567",
            style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: 16,
            ),
          ),
          SizedBox(
            height: 3.0,
          ),
          Text(
            'Kerja-kerja pembersihan dan selenggara am.',
            style: (TextStyle(fontSize: 16)),
          ),
          SizedBox(
            height: 18.0,
          ),
          Text(
            "Tarikh mula arahan kerja: 10 Mar 2020",
            style: TextStyle(
              color: Colors.black,
              letterSpacing: .7,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildForm() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 10.0, bottom: 2),
                child: Text('Pekerja Anda:'),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(8, 0, 8, 8),
                child: Text(
                  'Samad',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.blueGrey,
                      letterSpacing: 1,
                      fontStyle: FontStyle.italic),
                ),
              ),
            ],
          ),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 10.0, bottom: 8.0),
              child: Text('Kerja yang perlu disiapkan'),
            ),
            Padding(
              padding:
                  EdgeInsets.only(left: 8.0, right: 8.0, bottom: 10.0, top: 0),
              child: TextFormField(
                style: TextStyle(
                  fontSize: 14.0,
                  color: Colors.black87,
                ),
                decoration: InputDecoration(
                  fillColor: Colors.white,
                  contentPadding:
                      EdgeInsets.symmetric(vertical: 20, horizontal: 20.0),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide: BorderSide(),
                  ),
                ),
                keyboardType: TextInputType.multiline,
                maxLines: 2,
              ),
            ),
          ],
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 10.0, bottom: 8.0),
              child: Text('Lokasi'),
            ),
            Padding(
              padding:
                  EdgeInsets.only(left: 8.0, right: 8.0, bottom: 10.0, top: 0),
              child: TextFormField(
                style: TextStyle(
                  fontSize: 14.0,
                  color: Colors.black87,
                ),
                decoration: InputDecoration(
                  fillColor: Colors.white,
                  contentPadding:
                      EdgeInsets.symmetric(vertical: 20, horizontal: 20.0),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide: BorderSide(),
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget _buildButton() {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: SizedBox(
        width: double.infinity,
        child: RaisedButton.icon(
          onPressed: () {
            Navigator.of(context).pop();
          },
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          label: Text(
            'Simpan',
            style: TextStyle(
                fontFamily: 'BellotaText',
                color: Colors.white,
                letterSpacing: 3),
          ),
          icon: Padding(
            padding: EdgeInsets.all(10.0),
            child: Icon(
              Icons.save,
            ),
          ),
          textColor: Colors.white,
          splashColor: Colors.red.shade900,
          color: Colors.red.shade600,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Assignment Details",
          style: TextStyle(
            letterSpacing: 1,
            fontFamily: "Lato",
          ),
        ),
        backgroundColor: Colors.blue.shade700,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        centerTitle: true,
      ),
      drawer: MyDrawer(),
      body: ListView(
        children: <Widget>[
          Padding(
              padding: EdgeInsets.all(10.0),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 20,
                            ),
                            _buildInfoLine2(),
                            Divider(),
                            _buildForm(),
                            _buildButton(),
                          ],
                          crossAxisAlignment: CrossAxisAlignment.start,
                        ),
                      )
                    ],
                  )
                ],
              ))
        ],
      ),
    );
  }
}
