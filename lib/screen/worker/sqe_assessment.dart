import 'package:cma_app/screen/worker/check_in.dart';
import 'package:cma_app/screen/worker/update_sqe.dart';
import 'package:flutter/material.dart';

class SqeAssessment extends StatefulWidget {
  @override
  _SqeAssessmentState createState() => _SqeAssessmentState();
}


class _SqeAssessmentState extends State<SqeAssessment> {

  String _information = 'No Information Yet';

  void updateInformation(String information) {
    setState(() => _information = information);
  }



  Widget _buttonKemaskini() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(32, 5, 32, 8),
      child: SizedBox(
        width: double.infinity,
        child: RaisedButton.icon(
          onPressed: () {},
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          label: Text(
            'Kemaskini',
            style: TextStyle(
                fontFamily: 'BellotaText',
                color: Colors.white,
                letterSpacing: 3),
          ),
          icon: Padding(
            padding: EdgeInsets.all(10.0),
            child: Icon(
              Icons.save,
            ),
          ),
          textColor: Colors.white,
          splashColor: Colors.red.shade900,
          color: Colors.red.shade600,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('CMA Contractor'),
        backgroundColor: Colors.blue.shade700,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context).push(new MaterialPageRoute(
                builder: (BuildContext context) => CheckIn()));
          },
        ),

      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'Submit new SQE Assessment',
                style: TextStyle(
                    fontSize: 16, fontFamily: "Lato", letterSpacing: 1),
              ),
            ),
            Divider(),
            _buttonKemaskini(),
          ],
        ),
      ),
    );
  }
}
