
import 'package:cma_app/myDrawer_worker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class WorkerAssignmentDetails extends StatefulWidget {
  @override
  _WorkerAssignmentDetailsState createState() => _WorkerAssignmentDetailsState();
}

class _WorkerAssignmentDetailsState extends State<WorkerAssignmentDetails> {

  Widget _titleSection() {
    return Container(
      padding: const EdgeInsets.fromLTRB(32, 32, 32, 8),
      child: Row(
        children: [
          Expanded(
            /*1*/
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                /*2*/
                Chip(
                  backgroundColor: Colors.green,
                  label: Text(
                    'PO 454545445',
                    style: TextStyle(
                      fontFamily: "Lato",
                      color: Colors.white,
                    ),
                  ),
                ),
                Text(
                  'Kerja-kerja pembersihan dan selenggara am.',
                  style: TextStyle(
                    color: Colors.grey[500],
                  ),
                ),
              ],
            ),
          ),
          /*3*/
          Icon(
            Icons.star,
            color: Colors.red[500],
          ),
        ],
      ),
    );
  }

  Widget _textSection() {
    return Container(
        padding: const EdgeInsets.all(32),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            ListTile(
              contentPadding: EdgeInsets.all(0.0),
              dense: true,
              leading: Icon(Icons.calendar_today),
              title: Text('Tarikh mula arahan kerja (Kontrak PO)'),
              subtitle: Text('10 April 2020'),
            ),
            ListTile(
              contentPadding: EdgeInsets.all(0.0),
              dense: true,
              leading: Icon(FontAwesomeIcons.calendarCheck),
              title: Text('Delivery Date (Kontrak PO)'),
              subtitle: Text('10 April 2020'),
            ),
            ListTile(
              contentPadding: EdgeInsets.all(0.0),
              dense: true,
              leading: Icon(Icons.business_center),
              title: Text('Vendor '),
              subtitle: Text('Bina Maju Sdn. Bhd.'),
            ),
            ListTile(
              contentPadding: EdgeInsets.all(0.0),
              dense: true,
              leading: Icon(FontAwesomeIcons.calendarTimes),
              title: Text('Tarikh Rancang Mula Kerja (Workplan)'),
              subtitle: Text('15 Mar 2020'),
            ),
            ListTile(
              contentPadding: EdgeInsets.all(0.0),
              dense: true,
              leading: Icon(Icons.info_outline),
              title: Text('Maklumat Lain'),
              subtitle: Text('Limited Access due to MCO.'),
            ),
            ListTile(
              contentPadding: EdgeInsets.all(0.0),
              dense: true,
              leading: Icon(Icons.today),
              title: Text('Tarikh Penugasan:'),
              subtitle: Text('10 April 2020'),
            ),
            ListTile(
              contentPadding: EdgeInsets.all(0.0),
              dense: true,
              leading: Icon(FontAwesomeIcons.userCircle),
              title: Text('Nama Staff'),
              subtitle: Text('Samad'),
            ),
            ListTile(
              contentPadding: EdgeInsets.all(0.0),
              dense: true,
              leading: Icon(Icons.input),
              title: Text('Tarikh Check In: '),
              subtitle: Text('30 April 2020'),
            ),
          ],
        )
    );
  }

  Widget _buttonSection() {
    return Container(
        padding: const EdgeInsets.all(32),
        child: SizedBox(
          width: double.infinity,
          child: RaisedButton.icon(
            onPressed: () {
//              Navigator.of(context).pop();
//              Navigator.of(context).push(new MaterialPageRoute(
//                  builder: (BuildContext context) => UpdateSqe()));
            },
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(20.0))),
            label: Text(
              'Check In',
              style: TextStyle(
                  fontFamily: 'Lato',
                  color: Colors.white,
                  letterSpacing: 3),
            ),
            icon: Padding(
              padding: EdgeInsets.all(10.0),
              child: Icon(
                Icons.input,
              ),
            ),
            textColor: Colors.white,
            splashColor: Colors.red.shade900,
            color: Colors.red.shade600,
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: new Text(
          "CMA Contractor",
          style: TextStyle(color: Colors.blueAccent.shade700),
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
        iconTheme: IconThemeData(color: Colors.blueAccent.shade700),
      ),
      drawer: MyWorkerDrawer(),
      body: ListView(
        children: [
//          Image.asset(
//            'assets/images/picone.jpg',
//            width: 80,
//            height: 80,
//            fit: BoxFit.cover,
//          ),
          _titleSection(),
          _textSection(),
          _buttonSection()
        ],
      ),
    );
  }
}
