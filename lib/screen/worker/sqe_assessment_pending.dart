import 'package:cma_app/screen/worker/update_sqe.dart';
import 'package:flutter/material.dart';

class SqeAssessmentPending extends StatefulWidget {
  @override
  _SqeAssessmentState createState() => _SqeAssessmentState();
}

class _SqeAssessmentState extends State<SqeAssessmentPending> {

  Widget _buttonSection() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(32, 5, 32, 8),
      child: SizedBox(
        width: double.infinity,
        child: RaisedButton.icon(
          onPressed: () {
            Navigator.of(context).pop();       },
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          label: Text(
            'Kemaskini',
            style: TextStyle(
                fontFamily: 'BellotaText',
                color: Colors.white,
                letterSpacing: 3),
          ),
          icon: Padding(
            padding: EdgeInsets.all(10.0),
            child: Icon(
              Icons.edit,
            ),
          ),
          textColor: Colors.white,
          splashColor: Colors.red.shade900,
          color: Colors.red.shade600,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text('CMA Contractor'),
          backgroundColor: Colors.blue.shade700,
          centerTitle: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).push(new MaterialPageRoute(
                  builder: (BuildContext context) => UpdateSqe()));
            },
          ),

      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Chip(
              backgroundColor: Colors.orange,
              label: Text(
                'Pending Approval',
                style: TextStyle(
                  fontFamily: "Lato",
                  color: Colors.white,
                ),
              ),
            ),
          ),


          _buttonSection(),
        ],
      ),
    );
  }
}
