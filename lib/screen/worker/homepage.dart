
import 'package:cma_app/myDrawer_worker.dart';
import 'package:cma_app/screen/supervisor/timetable.dart';
import 'package:cma_app/screen/worker/timetable_worker.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';


class HomepageWorker extends StatefulWidget {
  @override
  _HomepageWorkerState createState() => _HomepageWorkerState();
}

class _HomepageWorkerState extends State<HomepageWorker> {

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
        title:  Text(
          "CMA Contractor",
        ),
        centerTitle: true,
        backgroundColor: Colors.blue.shade700,
      ),
      drawer: MyWorkerDrawer(),
      body: TimetableWorker(),

    );
  }
}

