import 'dart:convert';

import 'package:cma_app/screen/worker/assignment_details.dart';
import 'package:cma_app/screen/worker/homepage.dart';
import 'package:cma_app/screen/worker/worker_assignment_details.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WorkerPOActive extends StatefulWidget {
  @override
  _WorkerPOActiveState createState() => _WorkerPOActiveState();
}

class _WorkerPOActiveState extends State<WorkerPOActive> {
  Widget _buildContent(_data) {
    return InkWell(
      onTap: (){
        Navigator.of(context).push(MaterialPageRoute(
            builder: (BuildContext context) =>
                WorkerAssignmentDetails()));
      },
      highlightColor: Colors.white30,
      splashColor: Colors.blue.shade50,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: <Widget>[
                      Image.asset(
                        'assets/images/activepo.png',
                        height: 30,
                        width: 30,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          _data['id'],
                          style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold,
                              letterSpacing: 1),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 8, 8, 2),
                    child: Text(
                      _data['title'],
                      style: TextStyle(
                          fontSize: 15.0, fontWeight: FontWeight.w500),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 0, 8, 8),
                    child: Text(
                      "Tarikh mula arahan kerja: " + _data['startDate'],
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.grey.shade700,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.only(left: 4.0),
                    child: GestureDetector(
                      child: Icon(Icons.keyboard_arrow_right,
                          color: Colors.blue, size: 30.0),
                      onTap: () {
                        //go to PO  Work Plan page
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) =>
                                AssignmentDetails()));
                      },
                    )),
              ],
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Active PO",
          style: TextStyle(
            letterSpacing: 1,
            fontFamily: 'Lato',
          ),
        ),
        elevation: 0,
        centerTitle: true,
        backgroundColor: Colors.blue.shade700,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                      builder: (BuildContext context) => HomepageWorker()),
                );
              },
            );
          },
        ),

      ),
      body: FutureBuilder(
          future: DefaultAssetBundle.of(context).loadString('data/mydata.json'),
          builder: (context, snapshot) {
            var _data = jsonDecode(snapshot.data.toString());

            return ListView.builder(
              padding: EdgeInsets.all(8.0),
              itemCount: _data == null ? 0 : _data.length,
              itemBuilder: (BuildContext context, int index) {
                return Card(
                  elevation: 1,
                  child: _buildContent(_data[index]),
                );
              },
            );
          }),
    );
  }
}
