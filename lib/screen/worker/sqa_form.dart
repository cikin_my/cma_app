import 'dart:developer';

import 'package:cma_app/screen/worker/check_in.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';

class SqaForm extends StatefulWidget {
  @override
  _SqaFormState createState() => _SqaFormState();
}

class _SqaFormState extends State<SqaForm> {
  //initState

  String sqaStatus = "approve";
  bool selected = false;
  var checklistStatus = List<bool>();
  var checkedList = 0 ;

  Future<List<User>> _getData() async {
    var data = await http.get(
        "https://bitbucket.org/cikin_my/myjson/raw/9a14da0ada59db2fce8b4ef81cfdf85d6cb87b76/sqa_checklist.json");

    var jsonData = json.decode(data.body);
    List<User> users = [];

    for (var u in jsonData) {
      User user = User(u["id"], u["title"], u["description"]);

      users.add(user);
      checklistStatus.add(false);
    }

    checkedList = checklistStatus.where((item) => item == true).length;
    print(checklistStatus.where((item) => item == true).length);
    return users;
  }

  Widget chip(String label, Color color) {
    return Chip(
      labelPadding: EdgeInsets.fromLTRB(10, 0, 10, 0),
      label: Text(
        label,
        style: TextStyle(
            color: Colors.white, fontFamily: 'Lato', letterSpacing: 2),
      ),
      backgroundColor: color,
      elevation: 1.0,
      shadowColor: Colors.grey[60],
      padding: EdgeInsets.all(3.0),
    );
  }
  Widget _buttonSave() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(32, 5, 32, 8),
      child: SizedBox(
        width: double.infinity,
        child: RaisedButton.icon(
          onPressed: () {},
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          label: Text(
            'Simpan',
            style: TextStyle(
                fontFamily: 'Lato',
                color: Colors.white,
                letterSpacing: 3),
          ),
          icon: Padding(
            padding: EdgeInsets.all(10.0),
            child: Icon(
              Icons.save,
            ),
          ),
          textColor: Colors.white,
          splashColor: Colors.blue.shade900,
          color: Colors.blue.shade600,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('CSQA Assessment '),
          backgroundColor: Colors.blue.shade700,
          centerTitle: true,
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: 8,
              child: Container(
                padding: EdgeInsets.only(bottom: 18.0),
                child: FutureBuilder(
                  future: _getData(),
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    print(snapshot.data);
                    if (snapshot.data == null) {
                      return Container(
                          child: Center(child: Text("Loading...")));
                    } else {
                      return ListView.builder(
                        itemCount: snapshot.data.length,
                        itemBuilder: (BuildContext context, int index) {
                          if (index == 0) {
                            return Column(
                              children: <Widget>[
                                Padding(
                                    padding: const EdgeInsets.fromLTRB(8,0,8,0),
                                    child: sqaStatus != "new"
                                        ? Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text("Current Status of CSQA Assessement"),
                                        chip(sqaStatus, sqaStatus == "pending" ? Colors.orange
                                            : sqaStatus == "approve" ? Colors.green
                                            : Colors.white),
                                      ],
                                    )
                                        : Row(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text(
                                            "Submit New CSQA Assessment",
                                            style: TextStyle(
                                                fontFamily: 'Lato',
                                                letterSpacing: 1,
                                                fontSize: 17,
                                                fontWeight: FontWeight.w600,
                                                color: Colors.blueGrey.shade700),
                                          ),
                                        ),
                                      ],
                                    )
                                ),
                                Divider(),
                              ],
                            );

                          }
                          return Container(
                            decoration: BoxDecoration(
                              border: Border(
                                  bottom:
                                      BorderSide(color: Colors.grey.shade200)),
                            ),
                            child: ListTile(
                              title: Text(
                                snapshot.data[index -1].id +
                                    " . " +
                                    snapshot.data[index].title,
                                style: TextStyle(fontSize: 16),
                              ),
                              subtitle: snapshot.data[index].description == ""
                                  ? Container(
                                      child: TextFormField(
                                        style: TextStyle(
                                          fontSize: 17.0,
                                          color: Colors.black87,
                                        ),
                                        decoration: InputDecoration(
                                          alignLabelWithHint: true,
                                          labelText:
                                              "Sila masukan remaks sekiranya ada",
                                          fillColor: Colors.white,
                                          contentPadding: EdgeInsets.all(20),
                                          border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            borderSide: BorderSide(),
                                          ),
                                        ),
                                        keyboardType: TextInputType.multiline,
                                        maxLines: 3,
                                      ),
                                    )
                                  : Text(snapshot.data[index].description,
                                      style: TextStyle(fontFamily: 'Lato')),
                              isThreeLine: true,
                              trailing: snapshot.data[index].description == ""
                                  ? Icon(Icons.edit, color: Colors.blueGrey,)
                                  :Checkbox(
                                  value: checklistStatus[index],
                                  onChanged: (bool val) {
                                    setState(() {
                                      checklistStatus[index] = !checklistStatus[index];
                                    });
                                  }),
                              onTap: () {},
                            ),
                          );
                        },
                      );
                    }
                  },
                ),
              ),
            ),
            Expanded(
                flex: 0,
                child: Container(
                  padding: EdgeInsets.fromLTRB(8,18,8,8),
                  color: Colors.grey[100],
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text('JUMLAH SKOR : ', style: TextStyle(letterSpacing: 1, fontFamily: 'Lato', fontWeight: FontWeight.bold),),
                          Text(((checkedList /11) * 100).toStringAsFixed(0) , style: TextStyle(letterSpacing: 1, fontFamily: 'Lato', fontWeight: FontWeight.bold, color: Colors.red[800],fontSize: 17),),
                          Text(" /100", style: TextStyle(letterSpacing: 1, fontFamily: 'Lato', fontWeight: FontWeight.bold, fontSize: 17),),
                        ],
                      ),
                      SizedBox(height: 8,),
                      _buttonSave(),

                    ],
                  ),
                ))
          ],
        ),
    );
  }
}

class User {
  final String id;
  final String title;
  final String description;

  User(this.id, this.title, this.description);
}
