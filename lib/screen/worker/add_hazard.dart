import 'dart:convert';
import 'dart:ui';

import 'package:cma_app/screen/worker/check_in.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;

class Hazard_add extends StatefulWidget {
  @override
  _Hazard_addState createState() => _Hazard_addState();
}

class _Hazard_addState extends State<Hazard_add> {
  Users _currentUser;
  List thisIsmyData;

  final String uri = 'https://bitbucket.org/cikin_my/myjson/raw/3fc8a93f3814df493a03a6e190df4b448434cfc8/hazard_wth_cat.json';

  Future<List<Users>> _fetchUsers() async {
    var response = await http.get(uri);
    var jsonData = json.decode(response.body);

    //if (response.statusCode == 200) {
    final items = json.decode(response.body).cast<Map<String, dynamic>>();
    List<Users> listOfUsers = items.map<Users>((json) {
      return Users.fromJson(json);
    }).toList();

    return listOfUsers;
  }

  Widget _buildButton() {
    return RaisedButton.icon(
      onPressed: () {},
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(20.0))),
      label: Text(
        'Tambah',
        style: TextStyle(fontFamily: 'BellotaText', color: Colors.white, letterSpacing: 3),
      ),
      icon: Padding(
        padding: EdgeInsets.all(8.0),
        child: Icon(
          Icons.add,
        ),
      ),
      textColor: Colors.white,
      splashColor: Colors.blue.shade900,
      color: Colors.blue.shade600,
    );
  }

  Widget _buildPageHeader() {
    return Container(
      color: Colors.blue[900],
      width: double.infinity,
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: <Widget>[
          Icon(
            Icons.more_vert,
            color: Colors.red,
          ),
          Text(
            "PENGENALPASTIAN HAZARD  & LANGKAH KAWALAN ",
            style: TextStyle(fontFamily: 'Lato', color: Colors.blueGrey[50]),
          ),
        ],
      ),
    );
  }

  Widget _buildDropdownList() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(left: 18.0, right: 18.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            FutureBuilder<List<Users>>(
                future: _fetchUsers(),
                builder: (BuildContext context, AsyncSnapshot<List<Users>> snapshot) {
                  if (!snapshot.hasData) return CircularProgressIndicator();
                  return Container(
                    child: DropdownButton<Users>(
                      items: snapshot.data
                          .map((user) => DropdownMenuItem<Users>(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    ListTile(
                                      title: Text(user.Kod_hazard + "-" + user.jenis_hazard),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 0.0),
                                    ),
                                    Divider(),
                                  ],
                                ),
                                value: user,
                              ))
                          .toList(),
                      onChanged: (Users value) {
                        setState(() {
                          _currentUser = value;
                        });
                      },
                      iconSize: 30,
                      icon: Icon(
                        Icons.keyboard_arrow_down,
                        color: Colors.blue.shade700,
                      ),
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                      ),
                      isExpanded: true,
                      //value: _currentUser,
                      hint: Text(
                        'Pilih Hazard',
                        style: (TextStyle(fontWeight: FontWeight.w600)),
                      ),
                    ),
                    padding: EdgeInsets.all(8.0),
                  );
                }),
            SizedBox(height: 10.0),
            _currentUser != null
                ? Column(
                    children: <Widget>[
                      Card(
                        child: Column(
                          children: <Widget>[
                            Container(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  children: <Widget>[
                                    Text(
                                      "Jenis Hazard : ",
                                      style: TextStyle(letterSpacing: 1, color: Colors.blueGrey),
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(
                                      _currentUser.Kod_hazard + ". " + _currentUser.jenis_hazard,
                                      style: TextStyle(letterSpacing: .5, color: Colors.red[800], fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                )),
                            Container(
                                width: double.infinity,
                                color: Colors.grey[100],
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  children: <Widget>[
                                    Text(
                                      "Langkah Kawalan : ",
                                      style: TextStyle(letterSpacing: 1, color: Colors.blueGrey),
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(
                                      _currentUser.kod_kawalan + ". " + _currentUser.langkah_kawalan,
                                      style: TextStyle(
                                        color: Colors.black,
                                      ),
                                    )
                                  ],
                                ))
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      _buildButton(),
                      Divider(
                        thickness: 3,
                        color: Colors.grey[200],
                      ),
                    ],
                  )
                : Container(
                    width: double.infinity,
                    child: Padding(
                      padding: const EdgeInsets.all(18.0),
                      child: Center(
                          child: Text(
                        "- Sila pilih Hazard. -",
                        style: TextStyle(letterSpacing: 1),
                      )),
                    )),
          ],
        ),
      ),
    );
  }

  Widget _buildListSection(data) {
    return Padding(
      padding: const EdgeInsets.only(left: 15.0, right: 15.0),
      child: Card(
        elevation: .75,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                padding: EdgeInsets.all(8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Jenis Hazard",
                      style: TextStyle(fontWeight: FontWeight.bold, letterSpacing: .5),
                    ),
                    Text(
                      data['Kod_hazard'] + ". " + data['jenis_hazard'],
                      style: TextStyle(
                        fontSize: 16,
                        letterSpacing: .5,
                      ),
                    ),
                  ],
                )),
            SizedBox(
              height: 5,
            ),
            Container(
                padding: EdgeInsets.all(8),
                color: Colors.grey[50],
                width: double.infinity,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Langkah Kawalan",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        letterSpacing: .5,
                      ),
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          width: 4,
                        ),
                        Expanded(
                          child: Text(
                            data['kod_kawalan'] + ". " + data['langkah_kawalan'],
                            style: TextStyle(
                              fontSize: 16,
                              letterSpacing: .5,
                              color: Colors.blueGrey[600],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                )),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Hazard & Langkah Kawalan '),
        backgroundColor: Colors.blue.shade700,
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) => CheckIn()),
                );
              },
            );
          },
        ),
      ),
      body: FutureBuilder(
          // this supposed to be the api you call to get saved hazard.
          future: DefaultAssetBundle.of(context).loadString('data/hazardRecord.json'), //<--dummy file

          builder: (context, snapshot) {
            var _data = jsonDecode(snapshot.data.toString());
            return ListView.builder(
              itemCount: _data == null ? 0 : _data.length,
              itemBuilder: (BuildContext context, int index) {
                if (index == 0) {
                  return Column(
                    children: <Widget>[
                      _buildPageHeader(),
                      _buildDropdownList(),
                    ],
                  );
                }

                //this should be teh list for all selected hazard.
                return _buildListSection(_data[index]);
              },
            );
          }),
    );
  }
}

class Users {
  int id;
  String Kod_hazard;
  String jenis_hazard;
  String langkah_kawalan;
  String kod_kawalan;

  Users({
    this.id,
    this.Kod_hazard,
    this.jenis_hazard,
    this.langkah_kawalan,
    this.kod_kawalan,
  });

  factory Users.fromJson(Map<String, dynamic> json) {
    return Users(
      id: json['id'],
      Kod_hazard: json['Kod_hazard'],
      jenis_hazard: json['jenis_hazard'],
      kod_kawalan: json['kod_kawalan'],
      langkah_kawalan: json['langkah_kawalan'],
    );
  }
}
