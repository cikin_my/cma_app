import 'package:cma_app/pages/login_option-expandlist.dart';
import 'package:cma_app/screen/supervisor/homepage.dart';
import 'package:cma_app/screen/supervisor/timetable.dart';
import 'package:cma_app/screen/worker/homepage.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../main.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class LoginOption extends StatefulWidget {
  @override
  _LoginOptionState createState() => _LoginOptionState();

}

class _LoginOptionState extends State<LoginOption> {



  // To adjust the layout according to the screen size
  // so that our layout remains responsive ,we need to
  // calculate the screen height
  double screenHeight;
  // Set intial mode to login
  AuthMode _authMode = AuthMode.NON;

  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: Color.fromRGBO(242, 242, 242, 1),
      body: SingleChildScrollView(
        child: Stack(
          children: <Widget>[
            lowerHalf(context),
            upperHalf(context),
            pageTitle(),
            (_authMode == AuthMode.SUPERVISOR)?LoginAsSupervisor(context):(_authMode == AuthMode.WORKER)? LoginAsWorker(context): loginType(context),


Row(

)
          ],
        ),

      ),
    );
  }

  Widget pageTitle() {
    return Container(
      margin: EdgeInsets.only(top: screenHeight / 2.4),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new Expanded(
            child: new GestureDetector(
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
//                                t i t l e
                  new Padding(
                    padding: new EdgeInsets.only(
                        left: 8.0, right: 8.0, bottom: 8.0, top: 0.0),
                    child: new Text("CMA",
                      style: new TextStyle(
                        fontFamily: 'BellotaText',
                        fontSize: 30.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
//                                d e s c r i p t i o n
                  new Padding(
                    padding: new EdgeInsets.only(
                        left: 8.0, right: 4.0, bottom: 4.0),
                    child: new Text("Contractor Management Application for Contractors",
                      style: new TextStyle(
                          fontFamily: 'BellotaText',
                          fontSize:18.0,
                          fontWeight: FontWeight.normal
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),

                ],
              ),

            ),
          ),
        ],
      ),
    );
  }

  Widget loginType(BuildContext context) {
    return Column(
      children: <Widget>[
      
        Container(
          margin: EdgeInsets.only(top: screenHeight / 1.8),
          padding: EdgeInsets.only(left: 10, right: 10),
          child: Card(
            color: Colors.transparent,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            elevation: 0,
            child: Padding(
              padding: const EdgeInsets.all(30.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Login As : ",
                      style: TextStyle(
                        fontFamily: 'BellotaText',
                        color: Colors.black54,
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                        letterSpacing: 2
                        ,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),

                  SizedBox(
                    width: double.infinity,
                    child: RaisedButton.icon(
                      onPressed: (){
                        setState(() {
                          _authMode = AuthMode.SUPERVISOR;
                        });
                      },
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
                      label: Text('Supervisor',
                        style: TextStyle(
                            fontFamily: 'BellotaText',
                            color: Colors.white,
                            letterSpacing: 3),),
                      icon: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: FaIcon(FontAwesomeIcons.userTie),
                      ),
                      textColor: Colors.white,
                      splashColor: Colors.blue.shade700,
                      color: Colors.redAccent,
                      ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  SizedBox(
                    width: double.infinity,
                    child: RaisedButton.icon(
                      onPressed: (){ // Set intial mode to login
                        setState(() {
                          _authMode = AuthMode.WORKER;
                        });
                        },
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(20.0))),
                      label: Text('Worker',
                        style: TextStyle(
                            fontFamily: 'BellotaText',
                            color: Colors.white,
                            letterSpacing: 3),),
                      icon: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: FaIcon(FontAwesomeIcons.hardHat, color: Colors.white,),
                      ),
                      textColor: Colors.white,
                      splashColor: Colors.blue.shade700,
                      color: Colors.blue,),
                  )



                ],
              ),
            ),
          ),
        ),

        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 10,
            ),
            Image.asset("assets/images/tnb.png" , width: 90.0,height: 90.0,),
          ],
        )


      ],
    );
  }

  Widget upperHalf(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(55.0),
      height: screenHeight / 2,
      child: Image.asset(
        'assets/images/login2.jpg',
        fit: BoxFit.cover,
      ),
    );
  }

  Widget lowerHalf(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        height: screenHeight / 2,
        color: Color.fromRGBO(242, 242, 242, 1),
      ),

    );
  }

  Widget LoginAsSupervisor(BuildContext context) {
    return Column(

      children: <Widget>[
        Container(
          margin: EdgeInsets.only(top: screenHeight / 3),
          padding: EdgeInsets.only(left: 10, right: 10),
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            elevation: 8,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(30.0,10.0,30.0,30.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Supervisor",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 24,
                          fontWeight: FontWeight.w600,
                          letterSpacing: 3
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Login",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                        fontWeight: FontWeight.w400,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                        labelText: "ID Pengguna", hasFloatingPlaceholder: true),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                        labelText: "Kata Laluan", hasFloatingPlaceholder: true),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      MaterialButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                          Navigator.of(context).push(new MaterialPageRoute(
                              builder: (BuildContext context) => new LoginOption()));
                        },
                        child: Text("Back")
                      ),
                      Expanded(
                        child: Container(),
                      ),
                      FlatButton(
                        child: Text("Login"),
                        color: Color(0xFF4B9DFE),
                        textColor: Colors.white,
                        padding: EdgeInsets.only(
                            left: 38, right: 38, top: 15, bottom: 15),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5)),
                        onPressed: () {
                          Navigator.of(context).pop();
                          Navigator.of(context).push(new MaterialPageRoute(
                              builder: (BuildContext context) => Homepage()));
                        },
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 40,
            ),
            Text(
              "Don't have an account ?",
              style: TextStyle(color: Colors.grey),
            ),
            FlatButton(
              onPressed: () {

              },
              textColor: Colors.black87,
              child: Text("Create Account"),
            )
          ],
        )
      ],
    );
  }

  Widget LoginAsWorker(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(top: screenHeight / 5),
          padding: EdgeInsets.only(left: 10, right: 10),
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            elevation: 8,
            child: Padding(
              padding: const EdgeInsets.all(30.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Worker",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 24,
                          fontWeight: FontWeight.w600,
                          letterSpacing: 3
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Login",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                        fontWeight: FontWeight.w400,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                        labelText: "Kod Syarikat"),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                        labelText: "Kod Pekerja"),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      MaterialButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                            Navigator.of(context).push(new MaterialPageRoute(
                                builder: (BuildContext context) => LoginOption()));
                          },
                          child: Text("Back")
                      ),
                      Expanded(
                        child: Container(),
                      ),
                      FlatButton(
                        child: Text("Login"),
                        color: Color(0xFF4B9DFE),
                        textColor: Colors.white,
                        padding: EdgeInsets.only(
                            left: 38, right: 38, top: 15, bottom: 15),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5)),
                        onPressed: () {
                          Navigator.of(context).pop();
                          Navigator.of(context).push(new MaterialPageRoute(
                              builder: (BuildContext context) => HomepageWorker()));
                        },
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 40,
            ),
            Text(
              "Already have an account?",
              style: TextStyle(color: Colors.grey),
            ),
            FlatButton(
              onPressed: () {


              },
              textColor: Colors.black87,
              child: Text("Login"),
            )
          ],
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: FlatButton(
            child: Text(
              "Terms & Conditions",
              style: TextStyle(
                color: Colors.grey,
              ),
            ),
            onPressed: () {},
          ),
        ),
      ],
    );
  }

}


